package com.supermap.desktop.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.EmptyBorder;
import com.supermap.desktop.Application;
import com.supermap.desktop.controls.ControlsProperties;
import com.supermap.desktop.ui.controls.DialogResult;
import com.supermap.desktop.ui.controls.SmDialog;

public class DialogPassword extends SmDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPasswordField jpasswordField;
	private Component parent;
	private JLabel jlabelPasswordMessage;
	private JLabel jlabelPromptMessage;
	private JButton buttonOk;
	private JButton buttonCancel;
	private JPanel contentPanel;

	public DialogPassword() {
		super();
		init();
		this.setTitle(ControlsProperties.getString("String_Prompt"));
		this.setSize(280, 130);
		this.setDialogLocation(this.parent);
	}

	private void init() {
		this.jpasswordField = new JPasswordField();
		this.jpasswordField.setColumns(6);
		this.jlabelPasswordMessage = new JLabel();
		this.jlabelPasswordMessage.setForeground(Color.RED);
		this.jlabelPromptMessage = new JLabel();
		this.parent = (Component) Application.getActiveApplication().getMainFrame();
		this.contentPanel = new JPanel();
		this.buttonCancel = new JButton(ControlsProperties.getString("String_Button_Cancel"));
		this.buttonOk = new JButton(ControlsProperties.getString("String_Button_Ok"));

		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.getContentPane().add(this.contentPanel);
		GroupLayout groupContentPanel = new GroupLayout(this.contentPanel);
		this.contentPanel.setLayout(groupContentPanel);

		// @formatter:off
		groupContentPanel.setHorizontalGroup(groupContentPanel
				.createParallelGroup(Alignment.LEADING)
				.addGroup(groupContentPanel.createSequentialGroup()
						.addGroup(groupContentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(this.jlabelPromptMessage, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
								.addComponent(this.jpasswordField, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
								.addComponent(this.jlabelPasswordMessage, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)))
				.addGroup(groupContentPanel.createSequentialGroup().addGap(Integer.valueOf(ControlsProperties.getString("String_number")))
					    .addComponent(this.buttonOk).addGap(Integer.valueOf(ControlsProperties.getString("String_number")))
						.addComponent(this.buttonCancel)));
		groupContentPanel.setVerticalGroup(groupContentPanel
				.createSequentialGroup()
				.addGroup(groupContentPanel.createParallelGroup(Alignment.CENTER)
						.addComponent(this.jlabelPromptMessage, GroupLayout.PREFERRED_SIZE,GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE))
				.addGroup(groupContentPanel.createParallelGroup(Alignment.CENTER)
						.addComponent(this.jpasswordField, GroupLayout.PREFERRED_SIZE,GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE))
				.addGroup(groupContentPanel.createParallelGroup(Alignment.CENTER)
						.addComponent(this.jlabelPasswordMessage, GroupLayout.PREFERRED_SIZE,GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE))
				.addGroup(groupContentPanel.createParallelGroup(Alignment.CENTER).addGap(Integer.valueOf(ControlsProperties.getString("String_number")))
						.addComponent(this.buttonOk).addGap(Integer.valueOf(ControlsProperties.getString("String_number")))
						.addComponent(this.buttonCancel)));

		// @formatter:on

		this.buttonCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				canelButton_Click();
			}
		});
	}

	private void setDialogLocation(Component parent) {
		int parentWidth = parent.getWidth();
		int parentHeight = parent.getHeight();
		int dialogWidth = getWidth();
		int dialogHeight = getHeight();
		int width = (parentWidth - dialogWidth) / 2;
		int height = (parentHeight - dialogHeight) / 2;
		setLocation(width, height);
	}

	private void canelButton_Click() {
		try {
			this.dialogResult = DialogResult.CANCEL;
			this.setVisible(false);
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	public JPasswordField getJpasswordField() {
		return this.jpasswordField;
	}

	public void setJpasswordField(JPasswordField jpasswordField) {
		this.jpasswordField = jpasswordField;
	}

	public JLabel getJlabelPasswordMessage() {
		return this.jlabelPasswordMessage;
	}

	public void setJlabelPasswordMessage(JLabel jlabelPasswordMessage) {
		this.jlabelPasswordMessage = jlabelPasswordMessage;
	}

	public JLabel getJlabelPromptMessage() {
		return this.jlabelPromptMessage;
	}

	public void setJlabelPromptMessage(JLabel jlabelPromptMessage) {
		this.jlabelPromptMessage = jlabelPromptMessage;
	}

	public JButton getButtonOk() {
		return this.buttonOk;
	}

	public void setButtonOk(JButton okButton) {
		this.buttonOk = okButton;
	}

}
