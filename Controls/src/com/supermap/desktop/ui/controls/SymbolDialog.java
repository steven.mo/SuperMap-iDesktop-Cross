package com.supermap.desktop.ui.controls;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.supermap.data.GeoStyle;
import com.supermap.data.Resources;
import com.supermap.data.SymbolType;
import com.supermap.desktop.Application;
import com.supermap.desktop.controls.ControlsProperties;
import com.supermap.desktop.properties.CommonProperties;

/**
 * 符号选择器对话框
 * 
 * @author xuzw
 *
 */
public class SymbolDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	private transient Resources resources;

	private transient SymbolType fieldSymbolType;

	// 用于存放用户之前的GeoStyle
	private transient GeoStyle preStyle;

	// 操作过程中被赋值的GeoStyle，该GeoStyle经过用户一系列的赋值之后返回给用户
	private transient GeoStyle activeStyle;

	// 符号选择器的最终结果，默认是CANCEL
	private transient DialogResult dialogResult = DialogResult.CANCEL;

	private JPanel jPanelButton;

	private JButton jButtonOk;

	private JButton jButtonCancel;

	private transient SymbolPanel symbolPanel;

	/**
	 * 默认构造函数
	 */
	public SymbolDialog() {
		super();
		this.setModal(true);
	}

	/**
	 * 显示符号选择器
	 * 
	 * @param resources 资源
	 * @param geoStyle 用户传入的GeoStyle
	 * @param symbolType 符号选择器类型
	 * @return 返回用户对当前选择器的操作，确定按钮表示OK，取消按钮表示CANCLE
	 */
	public DialogResult showDialog(Resources resources, GeoStyle geoStyle, SymbolType symbolType) {
		// m_preStyle用于以后添加应用后点击取消的回滚数据操作
		preStyle = geoStyle.clone();
		this.activeStyle = geoStyle;
		this.resources = resources;
		this.fieldSymbolType = symbolType;

		if (fieldSymbolType.equals(SymbolType.MARKER)) {
			this.setSize(715, 530);
			initialize();
		} else if (fieldSymbolType.equals(SymbolType.LINE)) {
			this.setSize(720, 530);
			initialize();
		} else if (fieldSymbolType.equals(SymbolType.FILL)) {
			this.setSize(700, 530);
			initialize();
		}

		symbolPanel = new SymbolPanel(resources, geoStyle.clone(), symbolType);
		this.setLayout(new GridBagLayout());
		this.add(symbolPanel, new GridBagConstraintsHelper(0, 0).setFill(GridBagConstraints.BOTH).setWeight(1, 1));
		this.add(getButtonPanel(), new GridBagConstraintsHelper(0, 1).setInsets(10, 0, 10, 0).setWeight(1, 0).setAnchor(GridBagConstraints.EAST));
		this.setVisible(true);
		return dialogResult;
	}

	/**
	 * 获取用户经过一系列操作后最终的GeoStyle
	 * 
	 * @return
	 */
	public GeoStyle getStyle() {
		GeoStyle geoStyle = null;
		if (dialogResult.equals(DialogResult.CANCEL)) {
			geoStyle = preStyle;
		} else if (dialogResult.equals(DialogResult.OK)) {
			geoStyle = activeStyle;
		}
		return geoStyle;
	}

	/**
	 * 返回用户设置的资源
	 * 
	 * @return
	 */
	public Resources getResources() {
		return resources;
	}

	/**
	 * 返回用户设置的符号类型
	 * 
	 * @return
	 */
	public SymbolType getSymbolType() {
		return fieldSymbolType;
	}

	/**
	 * 初始化面板
	 */
	private void initialize() {
		this.setTitle(ControlsProperties.getString("String_Title_SymbolDialog"));
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		try {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension frameSize = this.getSize();
			if (frameSize.height > screenSize.height) {
				frameSize.height = screenSize.height;
			}
			if (frameSize.width > screenSize.width) {
				frameSize.width = screenSize.width;
			}
			this.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	/**
	 * 获取按钮面板
	 * 
	 * @return
	 */
	private JPanel getButtonPanel() {
		try {
			if (jPanelButton == null) {
				jPanelButton = new JPanel();
				jPanelButton.setLayout(new GridBagLayout());
				jButtonOk = new ControlButton();
				jButtonOk.setText(CommonProperties.getString("String_Button_OK"));
				jPanelButton.add(jButtonOk, new GridBagConstraintsHelper(0, 0).setAnchor(GridBagConstraints.EAST).setWeight(1, 0));
				jButtonOk.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						try {
							activeStyle = symbolPanel.getStyle();
							dialogResult = DialogResult.OK;
							setVisible(false);
						} catch (Exception e) {
							Application.getActiveApplication().getOutput().output(e);

						}
					}
				});

				jButtonCancel = new ControlButton();
				jButtonCancel.setText(CommonProperties.getString("String_Button_Cancel"));
				jPanelButton.add(jButtonCancel, new GridBagConstraintsHelper(1, 0).setAnchor(GridBagConstraints.CENTER).setInsets(0, 20, 0, 20));
				jButtonCancel.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						// 回滚到最初状态
						dialogResult = DialogResult.CANCEL;
						setVisible(false);
					}
				});
			}
		} catch (Exception e) {
			Application.getActiveApplication().getOutput().output(e);
		}
		return jPanelButton;
	}
}
