package com.supermap.desktop.ui.controls;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import com.supermap.data.DatasetType;
import com.supermap.data.GeoLine;
import com.supermap.data.GeoPoint;
import com.supermap.data.GeoRegion;
import com.supermap.data.GeoStyle;
import com.supermap.data.GeoText;
import com.supermap.data.Geometry;
import com.supermap.data.Point2D;
import com.supermap.data.Point2Ds;
import com.supermap.data.Resources;
import com.supermap.data.TextPart;
import com.supermap.data.Toolkit;
import com.supermap.mapping.Layer;
import com.supermap.mapping.ThemeGridRangeItem;
import com.supermap.mapping.ThemeGridUniqueItem;
import com.supermap.mapping.ThemeLabelItem;
import com.supermap.mapping.ThemeRangeItem;
import com.supermap.mapping.ThemeUniqueItem;

/**
 * 二维专题图子项节点装饰器
 * @author xuzw
 *
 */
class ThemeItemGeneralNodeDecorator implements TreeNodeDecorator {

	public void decorate(JLabel label, TreeNodeData data) {
		Layer parentLayer = data.getParentLayer();
		Resources resources = parentLayer.getDataset().getDatasource()
				.getWorkspace().getResources();
		Object item = data.getData();
		BufferedImage bufferedImage = new BufferedImage(IMAGEICON_WIDTH,
				IMAGEICON_HEIGHT, BufferedImage.TYPE_INT_ARGB);
		Graphics graphics = bufferedImage.getGraphics();
		ImageIcon icon = (ImageIcon) label.getIcon();
		NodeDataType type = data.getType();
		if (type.equals(NodeDataType.THEME_GRID_UNIQUE_ITEM)) {
			ThemeGridRangeItem gridRangeItem = (ThemeGridRangeItem) item;
			label.setText(gridRangeItem.getCaption());
			Color color = gridRangeItem.getColor();
			Geometry geometry = getGeometryByDatasetType(parentLayer
					.getDataset().getType());
			GeoStyle geoStyle = new GeoStyle();
			geoStyle.setFillBackColor(color);
			geoStyle.setFillForeColor(color);
			geoStyle.setLineColor(color);
			geometry.setStyle(geoStyle);
			Toolkit.draw(geometry, resources, graphics);
			icon.setImage(bufferedImage);
		} else if (type.equals(NodeDataType.THEME_GRID_UNIQUE_ITEM)) {
			ThemeGridUniqueItem gridUniqueItem = (ThemeGridUniqueItem) item;
			label.setText(gridUniqueItem.getCaption());
			Color color = gridUniqueItem.getColor();
			Geometry geometry = getGeometryByDatasetType(parentLayer
					.getDataset().getType());
			GeoStyle geoStyle = new GeoStyle();
			geoStyle.setFillBackColor(color);
			geoStyle.setFillForeColor(color);
			geoStyle.setLineColor(color);
			geometry.setStyle(geoStyle);
			Toolkit.draw(geometry, resources, graphics);
			icon.setImage(bufferedImage);
		} else if (type.equals(NodeDataType.THEME_LABEL_ITEM)) {
			ThemeLabelItem labelItem = (ThemeLabelItem) item;
			label.setText(labelItem.getCaption());
			TextPart part = new TextPart();
			part.setText(labelItem.getCaption());
			GeoText geoText = new GeoText(part);
			geoText.setTextStyle(labelItem.getStyle());
			Toolkit.draw(geoText, resources, graphics);
			icon.setImage(bufferedImage);
		} else if (type.equals(NodeDataType.THEME_RANGE_ITEM)) {
			ThemeRangeItem rangeItem = (ThemeRangeItem) item;
			label.setText(rangeItem.getCaption());
			GeoStyle geoStyle = rangeItem.getStyle();
			Geometry geometry = getGeometryByDatasetType(parentLayer
					.getDataset().getType());
			GeoStyle geoStyle2 = geoStyle.clone();
			if (geoStyle2.getLineWidth() > 0.2) {
				geoStyle2.setLineWidth(0.2);
			}
			geometry.setStyle(geoStyle2);
			Toolkit.draw(geometry, resources, graphics);
			icon.setImage(bufferedImage);
		} else if (item instanceof ThemeUniqueItem) {
			ThemeUniqueItem uniqueItem = (ThemeUniqueItem) item;
			label.setText(uniqueItem.getCaption());
			GeoStyle geoStyle = uniqueItem.getStyle();
			Geometry geometry = getGeometryByDatasetType(parentLayer
					.getDataset().getType());
			GeoStyle geoStyle2 = geoStyle.clone();
			if (geoStyle2.getLineWidth() > 0.2) {
				geoStyle2.setLineWidth(0.2);
			}
			geometry.setStyle(geoStyle2);
			Toolkit.draw(geometry, resources, graphics);
			icon.setImage(bufferedImage);
		}
	}

	private Geometry getGeometryByDatasetType(DatasetType type) {
		if (type.equals(DatasetType.POINT)) {
			GeoPoint geoPoint = new GeoPoint();
			return geoPoint;
		}
		if (type.equals(DatasetType.LINE)) {
			Point2D[] pts = { new Point2D(0, 16), new Point2D(4, 0),
					new Point2D(12, 16), new Point2D(16, 0) };
			Point2Ds ds = new Point2Ds(pts);
			GeoLine geoLine = new GeoLine(ds);
			return geoLine;
		}
		if (type.equals(DatasetType.REGION) || type.equals(DatasetType.GRID)) {
			Point2D[] points = { new Point2D(1, 15), new Point2D(1, 5),
					new Point2D(10, 1), new Point2D(15, 5),
					new Point2D(15, 15), new Point2D(1, 15) };
			Point2Ds ds = new Point2Ds(points);
			GeoRegion geoRegion = new GeoRegion(ds);
			return geoRegion;
		}
		return null;
	}

}
