package com.supermap.desktop.progress.Interface;

import java.util.concurrent.CancellationException;

/**
 * 用来更新进度信息接口
 * 
 * @author highsad
 *
 */
public interface IUpdateProgress {
	/**
	 * 更新进度信息
	 * 
	 * @param percent
	 *            进度
	 * @param remainTime
	 *            剩余时间
	 * @param message
	 *            进度信息
	 */
	void updateProgress(int percent, String remainTime, String message) throws CancellationException;

	/**
	 * 更新进度信息
	 * 
	 * @param percent
	 *            进度
	 * @param totalPercent
	 *            总进度
	 * @param remainTime
	 *            剩余时间
	 * @param message
	 *            进度信息
	 */
	void updateProgress(int percent, int totalPercent, String remainTime, String message) throws CancellationException;
}
