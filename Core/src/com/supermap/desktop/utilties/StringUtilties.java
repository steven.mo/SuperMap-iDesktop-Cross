package com.supermap.desktop.utilties;

import com.supermap.desktop.Application;

public class StringUtilties {
	private StringUtilties() {
		// 工具类不提供构造函数
	}

	public static boolean isNullOrEmptyString(Object obj) {
		if (obj == null) {
			return true;
		}

		if (!(obj instanceof String)) {
			return true;
		}

		return ((String) obj).isEmpty();
	}

	public static boolean isNullOrEmpty(String str) {
		return str == null || str.isEmpty();
	}

	public static boolean equals(String str1, String str2) {
		return equals(str1, str2, true);
	}

	public static boolean equals(String str1, String str2, boolean ignoreCase) {
		boolean result = false;

		try {
			// 同时为空，视为相同。null 和 空字符视为相同。
			if (isNullOrEmpty(str1) && isNullOrEmpty(str2)) {
				return true;
			}

			// 执行到这里时，表示不同时为空
			// 如果 str1 不为空，正常比较，得到结果
			// else str1 为空，那么 str2 一定不为空，result 保持 false 即可
			if (!isNullOrEmpty(str1)) {
				if (ignoreCase) {
					result = str1.equalsIgnoreCase(str2);
				} else {
					result = str1.equals(str2);
				}
			}
		} catch (Exception e) {
			Application.getActiveApplication().getOutput().output(e);
		}
		return result;
	}
}
