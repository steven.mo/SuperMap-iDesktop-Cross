package com.supermap.desktop.util;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;

import javax.swing.JTable;

import com.supermap.data.Datasource;
import com.supermap.data.conversion.DataImport;
import com.supermap.data.conversion.ImportResult;
import com.supermap.data.conversion.ImportSetting;
import com.supermap.data.conversion.ImportSettingWOR;
import com.supermap.data.conversion.ImportSettings;
import com.supermap.data.conversion.ImportSteppedEvent;
import com.supermap.data.conversion.ImportSteppedListener;
import com.supermap.desktop.Application;
import com.supermap.desktop.ImportFileInfo;
import com.supermap.desktop.dataconversion.DataConversionProperties;
import com.supermap.desktop.progress.Interface.UpdateProgressCallable;
import com.supermap.desktop.ui.UICommonToolkit;

public class DataImportCallable extends UpdateProgressCallable {
	private ArrayList<ImportFileInfo> fileInfos;
	private JTable table;

	public DataImportCallable(List<ImportFileInfo> fileInfos, JTable table) {
		this.fileInfos = (ArrayList<ImportFileInfo>) fileInfos;
		this.table = table;
	}

	@Override
	public Boolean call() {
		try {
			for (int i = 0; i < fileInfos.size(); i++) {
				final DataImport dataImport = new DataImport();
				ImportSettings importSettings = dataImport.getImportSettings();
				ImportFileInfo fileInfo = fileInfos.get(i);
				ImportSetting importSetting = fileInfo.getImportSetting();
				importSetting.setSourceFilePath(fileInfo.getFilePath());
				Datasource tempDatasource = fileInfo.getTargetDatasource();
				if (null != tempDatasource) {
					importSetting.setTargetDatasource(tempDatasource);
				} else {
					Datasource datasource = Application.getActiveApplication().getActiveDatasources()[0];
					importSetting.setTargetDatasource(datasource);
				}
				if (importSetting instanceof ImportSettingWOR) {
					((ImportSettingWOR) importSetting).setTargetWorkspace(Application.getActiveApplication().getWorkspace());
				}
				importSettings.add(importSetting);

				dataImport.addImportSteppedListener(new PercentProgress(i));

				ImportResult result = dataImport.run();
				printMessage(result, i);
				// 更新行
				((FileInfoModel) table.getModel()).updateRows(fileInfos);
			}

		} catch (Exception e2) {
			Application.getActiveApplication().getOutput().output(e2);
		}
		return true;
	}

	/**
	 * 进度事件得到运行时间
	 * 
	 * @author Administrator
	 *
	 */
	class PercentProgress implements ImportSteppedListener {
		private int i;

		public PercentProgress(int i) {
			this.i = i;
		}

		@Override
		public void stepped(ImportSteppedEvent arg0) {
			try {
				double count = fileInfos.size();
				int totalPercent = (int) (((i + 0.0) / count) * 100);
				updateProgressTotal(arg0.getSubPercent(), totalPercent, MessageFormat.format(
						DataConversionProperties.getString("String_ProgressControl_TotalImportProgress"), String.valueOf(totalPercent),
						String.valueOf(fileInfos.size())), MessageFormat.format(DataConversionProperties.getString("String_FileInput"), arg0.getCurrentTask()
						.getSourceFilePath()));
			} catch (CancellationException e) {
				arg0.setCancel(true);
			}
		}
	}

	/**
	 * 打印导入信息
	 * 
	 * @param result
	 * @param i
	 */
	private void printMessage(ImportResult result, int i) {
		ImportSetting[] successImportSettings = result.getSucceedSettings();
		ImportSetting[] failImportSettings = result.getFailedSettings();
		String successImportInfo = DataConversionProperties.getString("String_FormImport_OutPutInfoOne");
		String failImportInfo = DataConversionProperties.getString("String_FormImport_OutPutInfoTwo");
		if (null != successImportSettings && 0 < successImportSettings.length) {
			fileInfos.get(i).setState(DataConversionProperties.getString("String_FormImport_Succeed"));
			// 导入成功提示信息
			ImportSetting sucessSetting = successImportSettings[0];
			Application
					.getActiveApplication()
					.getOutput()
					.output(MessageFormat.format(successImportInfo, sucessSetting.getSourceFilePath(), "->", sucessSetting.getTargetDatasetName(),
							sucessSetting.getTargetDatasource().getAlias(), ""));
			UICommonToolkit.refreshSelectedDatasourceNode(sucessSetting.getTargetDatasource().getAlias());
		} else if (null != failImportSettings && 0 < failImportSettings.length) {
			fileInfos.get(i).setState(DataConversionProperties.getString("String_FormImport_NotSucceed"));
			Application.getActiveApplication().getOutput().output(MessageFormat.format(failImportInfo, failImportSettings[0].getSourceFilePath(), ">", ""));
		}
	}

}
