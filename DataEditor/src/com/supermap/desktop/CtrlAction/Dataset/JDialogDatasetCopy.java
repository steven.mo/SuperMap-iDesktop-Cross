package com.supermap.desktop.CtrlAction.Dataset;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JToolBar;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.SwingConstants;

import com.supermap.data.Charset;
import com.supermap.data.ColorSpaceType;
import com.supermap.data.Dataset;
import com.supermap.data.DatasetGrid;
import com.supermap.data.DatasetGridCollection;
import com.supermap.data.DatasetImage;
import com.supermap.data.DatasetImageCollection;
import com.supermap.data.DatasetType;
import com.supermap.data.DatasetVector;
import com.supermap.data.Datasource;
import com.supermap.data.EncodeType;
import com.supermap.desktop.Application;
import com.supermap.desktop.CommonToolkit;
import com.supermap.desktop.dataeditor.DataEditorProperties;
import com.supermap.desktop.properties.CommonProperties;
import com.supermap.desktop.ui.controls.CommonTableRender;
import com.supermap.desktop.ui.controls.DataCell;
import com.supermap.desktop.ui.controls.DatasourceComboBox;
import com.supermap.desktop.ui.controls.DialogResult;
import com.supermap.desktop.ui.controls.SmDialog;
import com.supermap.desktop.ui.controls.comboBox.UIComboBox;
import com.supermap.desktop.ui.controls.mutiTable.DDLExportTableModel;
import com.supermap.desktop.ui.controls.mutiTable.component.ComboBoxCellEditor;
import com.supermap.desktop.ui.controls.mutiTable.component.MutiTable;
import com.supermap.desktop.ui.controls.progress.FormProgressTotal;

import javax.swing.JCheckBox;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;

public class JDialogDatasetCopy extends SmDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final int COLUMN_INDEX_Dataset = 0;
	private static final int COLUMN_INDEX_CurrentDatasource = 1;
	private static final int COLUMN_INDEX_TargetDatasource = 2;
	private static final int COLUMN_INDEX_TargetDataset = 3;
	private static final int COLUMN_INDEX_EncodeType = 4;
	private static final int COLUMN_INDEX_Charset = 5;

	final JPanel contentPanel = new JPanel();
	private JToolBar toolBar;
	private JButton buttonSelectAll;
	private JButton buttonSelectInvert;
	private JButton buttonDelete;
	private JButton buttonSetting;
	private MutiTable table;
	private JCheckBox chckbxAutoClose;
	private JButton buttonOk;
	private JButton buttonCancel;
	private JButton buttonAdd;
	private transient Dataset[] datasets;

	/**
	 * Create the dialog.
	 */
	public JDialogDatasetCopy() {
		initComponents();
	}

	public JDialogDatasetCopy(Dataset[] datasets) {
		this.datasets = datasets;
		initComponents();
		initializeCopyInfo();
	}

	public void initComponents() {
		this.setModal(true);
		setTitle("Template");
		setBounds(100, 100, 677, 405);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		this.toolBar = new JToolBar();
		this.toolBar.setFloatable(false);

		this.table = new MutiTable();
		this.table.setRowHeight(23);
		this.table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		@SuppressWarnings("serial")
		DDLExportTableModel tableModel = new DDLExportTableModel(new String[] { "Dataset", "CurrentDatasource", "TargetDatasource", "TargetDataset",
				"EncodeType", "Charset" }) {
			boolean[] columnEditables = new boolean[] { false, false, true, true, true, true };

			@Override
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		};
		this.table.setModel(tableModel);
		this.table.putClientProperty("terminateEditOnFocusLost", true);
		initializeColumns();

		JScrollPane scrollPaneTable = new JScrollPane(table);
		this.table.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				tableOrScrollPanel_pressed(e);
			}
		});
		this.table.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DELETE) {
					deleteSelectedRow();
					setButtonState();
				}
			}
		});
		scrollPaneTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				tableOrScrollPanel_pressed(e);
			}
		});
		// @formatter:off
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(toolBar, GroupLayout.DEFAULT_SIZE, 528, Short.MAX_VALUE)
				.addComponent(scrollPaneTable, GroupLayout.DEFAULT_SIZE, 528, Short.MAX_VALUE)
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(7)
					.addComponent(toolBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPaneTable, GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE))
		);
		// @formatter:on
		buttonSelectAll = new JButton();
		buttonSelectAll.setEnabled(false);
		buttonSelectAll.setIcon(new ImageIcon(JDialogDatasetCopy.class
				.getResource("/com/supermap/desktop/coreresources/ToolBar/Image_ToolButton_SelectAll.png")));
		buttonSelectAll.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				buttonSelectAll_Click();
			}
		});

		buttonAdd = new JButton();
		buttonAdd.setIcon(new ImageIcon(JDialogDatasetCopy.class.getResource("/com/supermap/desktop/coreresources/ToolBar/Image_ToolButton_AddMap.png")));
		buttonAdd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				buttonAdd_Click();
				setButtonState();
			}
		});
		toolBar.add(buttonAdd);
		toolBar.add(buttonSelectAll);

		buttonSelectInvert = new JButton();
		buttonSelectInvert.setEnabled(false);
		buttonSelectInvert.setIcon(new ImageIcon(JDialogDatasetCopy.class
				.getResource("/com/supermap/desktop/coreresources/ToolBar/Image_ToolButton_SelectInverse.png")));
		buttonSelectInvert.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				buttonSelectInvert_Click();
			}
		});
		this.toolBar.add(buttonSelectInvert);
		this.toolBar.add(createSeparator());
		this.buttonDelete = new JButton();
		this.buttonDelete.setEnabled(false);
		this.buttonDelete
				.setIcon(new ImageIcon(JDialogDatasetCopy.class.getResource("/com/supermap/desktop/coreresources/ToolBar/Image_ToolButton_Delete.png")));
		this.buttonDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				buttonDelete_Click();
			}
		});
		this.buttonDelete.setHorizontalAlignment(SwingConstants.LEFT);
		this.toolBar.add(buttonDelete);
		this.toolBar.add(createSeparator());

		this.buttonSetting = new JButton();
		this.buttonSetting.setEnabled(false);
		this.buttonSetting.setIcon(new ImageIcon(JDialogDatasetCopy.class
				.getResource("/com/supermap/desktop/coreresources/ToolBar/Image_ToolButton_Setting.PNG")));
		this.buttonSetting.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				buttonSetting_Click();
			}
		});
		this.toolBar.add(buttonSetting);
		this.contentPanel.setLayout(gl_contentPanel);

		JPanel buttonPane = new JPanel();
		getContentPane().add(buttonPane, BorderLayout.SOUTH);

		this.chckbxAutoClose = new JCheckBox();
		this.chckbxAutoClose.setVerticalAlignment(SwingConstants.TOP);
		this.chckbxAutoClose.setHorizontalAlignment(SwingConstants.LEFT);
		this.chckbxAutoClose.setSelected(true);
		this.buttonOk = new JButton("OK");
		this.buttonOk.setEnabled(false);
		this.buttonOk.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButton_Click();
			}
		});
		this.buttonOk.setToolTipText("");
		this.buttonOk.setActionCommand("OK");
		getRootPane().setDefaultButton(buttonOk);

		this.buttonCancel = new JButton("Cancel");
		this.buttonCancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButton_Click();
			}
		});
		this.buttonCancel.setToolTipText("");
		this.buttonCancel.setActionCommand("Cancel");

		// @formatter:off
		GroupLayout gl_buttonPane = new GroupLayout(buttonPane);
		gl_buttonPane.setHorizontalGroup(
			gl_buttonPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_buttonPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(chckbxAutoClose)
					.addPreferredGap(ComponentPlacement.RELATED, 227, Short.MAX_VALUE)
					.addComponent(buttonOk, 75, 75, 75)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(buttonCancel, 75, 75, 75)
					.addContainerGap())
		);
		gl_buttonPane.setVerticalGroup(
			gl_buttonPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_buttonPane.createSequentialGroup()
					.addGap(5)
					.addGroup(gl_buttonPane.createParallelGroup(Alignment.BASELINE, false)
						.addComponent(buttonOk)
						.addComponent(chckbxAutoClose)
						.addComponent(buttonCancel))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		// @formatter:on
		buttonPane.setLayout(gl_buttonPane);
		table.getColumnModel().getColumn(COLUMN_INDEX_TargetDatasource).setPreferredWidth(40);
		initializeResources();
		this.setLocationRelativeTo(null);
	}

	private JToolBar.Separator createSeparator() {
		JToolBar.Separator separator = new JToolBar.Separator();
		separator.setOrientation(SwingConstants.VERTICAL);
		return separator;
	}

	/**
	 * 双击table或JScrollPanel打开数据集选择界面
	 * 
	 * @param e
	 */
	private void tableOrScrollPanel_pressed(MouseEvent e) {
		try {
			if (2 == e.getClickCount() && COLUMN_INDEX_TargetDataset != table.getSelectedColumn() && COLUMN_INDEX_EncodeType != table.getSelectedColumn()) {
				initializeDatasetChooser();
				setButtonState();
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	/**
	 * 通过按钮初始化数据集选择界面
	 * 
	 * @param e
	 */
	private void buttonAdd_Click() {
		try {
			initializeDatasetChooser();
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	/**
	 * 初始化数据集选择界面
	 */
	private void initializeDatasetChooser() {
		if (Application.getActiveApplication().getActiveDatasources().length > 0) {
			Datasource datasource = Application.getActiveApplication().getActiveDatasources()[0];
			DatasetChooserDataEditor dataSetChooser = new DatasetChooserDataEditor(this, datasource, table, true);
			dataSetChooser.setVisible(true);
		}
	}

	private void setButtonState() {
		boolean hasCopyDataset = false;
		if (0 < table.getRowCount()) {
			hasCopyDataset = true;
		}
		if (hasCopyDataset) {
			buttonDelete.setEnabled(true);
			buttonSelectAll.setEnabled(true);
			buttonSelectInvert.setEnabled(true);
			buttonSetting.setEnabled(true);
			buttonOk.setEnabled(true);
		} else {
			buttonDelete.setEnabled(false);
			buttonSelectAll.setEnabled(false);
			buttonSelectInvert.setEnabled(false);
			buttonSetting.setEnabled(false);
			buttonOk.setEnabled(false);
		}
	}

	public boolean isAutoClose() {
		return this.chckbxAutoClose.isSelected();
	}

	/**
	 * 初始化复制数据集界面中的table显示
	 */
	private void initializeCopyInfo() {
		try {
			for (int i = 0; i < datasets.length; i++) {
				Dataset dataset = datasets[i];
				Object[] datas = new Object[6];
				String imagePath = CommonToolkit.DatasetImageWrap.getImageIconPath(dataset.getType());
				DataCell dataCell = new DataCell(imagePath, dataset.getName());
				datas[COLUMN_INDEX_Dataset] = dataCell;
				Datasource datasource = Application.getActiveApplication().getActiveDatasources()[0];
				String datasourceImage = CommonToolkit.DatasourceImageWrap.getImageIconPath(datasource.getEngineType());
				datas[COLUMN_INDEX_CurrentDatasource] = new DataCell(datasourceImage, datasource.getAlias());
				String path = CommonToolkit.DatasourceImageWrap.getImageIconPath(datasource.getEngineType());
				DataCell datasourceCell = new DataCell(path, datasource.getAlias());
				datas[COLUMN_INDEX_TargetDatasource] = datasourceCell;
				datas[COLUMN_INDEX_TargetDataset] = datasource.getDatasets().getAvailableDatasetName(dataset.getName());
				datas[COLUMN_INDEX_EncodeType] = CommonProperties.getString("String_EncodeType_None");
				datas[COLUMN_INDEX_Charset] = "UTF8";
				this.table.addRow(datas);
				if (dataset.getType() == DatasetType.POINT) {
					table.getModel().setValueAt(CommonProperties.getString("String_EncodeType_None"), i, COLUMN_INDEX_EncodeType);
				}
			}
			if (0 < table.getRowCount()) {
				table.setRowSelectionInterval(0, table.getRowCount() - 1);
			}
			setButtonState();
		} catch (Exception e) {
			Application.getActiveApplication().getOutput().output(e);
		}
	}

	/**
	 * 资源化
	 */
	private void initializeResources() {
		try {
			this.setTitle(DataEditorProperties.getString("String_CopyDataset"));
			this.buttonCancel.setText(CommonProperties.getString("String_Button_Cancel"));
			this.buttonOk.setText(DataEditorProperties.getString("String_Copy"));

			table.getColumnModel().getColumn(COLUMN_INDEX_Dataset).setHeaderValue(CommonProperties.getString("String_ColumnHeader_SourceDataset"));
			table.getColumnModel().getColumn(COLUMN_INDEX_CurrentDatasource).setHeaderValue(CommonProperties.getString("String_ColumnHeader_SourceDatasource"));
			table.getColumnModel().getColumn(COLUMN_INDEX_TargetDatasource).setHeaderValue(CommonProperties.getString("String_ColumnHeader_TargetDatasource"));
			table.getColumnModel().getColumn(COLUMN_INDEX_TargetDataset).setHeaderValue(CommonProperties.getString("String_ColumnHeader_TargetDataset"));
			table.getColumnModel().getColumn(COLUMN_INDEX_EncodeType).setHeaderValue(CommonProperties.getString("String_ColumnHeader_EncodeType"));
			table.getColumnModel().getColumn(COLUMN_INDEX_Charset).setHeaderValue(DataEditorProperties.getString("String_Charset"));

			buttonAdd.setToolTipText(CommonProperties.getString("String_Add"));
			buttonSelectAll.setToolTipText(CommonProperties.getString("String_ToolBar_SelectAll"));
			buttonSelectInvert.setToolTipText(CommonProperties.getString("String_ToolBar_SelectInverse"));
			buttonDelete.setToolTipText(CommonProperties.getString("String_Delete"));
			buttonSetting.setToolTipText(CommonProperties.getString("String_ToolBar_Advanced"));
			this.chckbxAutoClose.setText(CommonProperties.getString("String_CheckBox_CloseDialog1"));
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	/**
	 * 初始化下拉框信息
	 */
	@SuppressWarnings("unchecked")
	private void initializeColumns() {
		try {
			// 数据源
			final DatasourceComboBox targetBox = new DatasourceComboBox(Application.getActiveApplication().getWorkspace().getDatasources());
			targetBox.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						// 选择不同的数据源时更新要复制的数据集的名称
						String item = (String) targetBox.getSelectItem();
						Datasource datasource = Application.getActiveApplication().getWorkspace().getDatasources().get(item);
						String dataset = table.getValueAt(table.getSelectedRow(), COLUMN_INDEX_Dataset).toString();
						table.getModel().setValueAt(datasource.getDatasets().getAvailableDatasetName(dataset), table.getSelectedRow(),
								COLUMN_INDEX_TargetDataset);
					} catch (Exception ex) {
						Application.getActiveApplication().getOutput().output(ex);
					}
				}
			});
			TableColumn targetDatasourceColumn = table.getColumnModel().getColumn(COLUMN_INDEX_TargetDatasource);
			TableCellEditor targetDatasourceCellEditor = new DefaultCellEditor(targetBox);
			targetDatasourceColumn.setCellEditor(targetDatasourceCellEditor);

			CommonTableRender renderer = new CommonTableRender();
			TableColumn sourceDatasetColumn = table.getColumnModel().getColumn(COLUMN_INDEX_Dataset);
			TableColumn currentDatasourceColumn = table.getColumnModel().getColumn(COLUMN_INDEX_CurrentDatasource);
			targetDatasourceColumn.setCellRenderer(renderer);
			sourceDatasetColumn.setCellRenderer(renderer);
			currentDatasourceColumn.setCellRenderer(renderer);
			// 编码类型
			ArrayList<String> encodeType = new ArrayList<String>();
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.NONE));
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.BYTE));
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.INT16));
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.INT24));
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.INT32));
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.DCT));
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.SGL));
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.LZW));
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.PNG));
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.COMPOUND));
			final JComboBox<String> encodeTypeComboBox = new JComboBox<String>(new DefaultComboBoxModel<String>(
					encodeType.toArray(new String[encodeType.size()])));

			encodeTypeComboBox.addPopupMenuListener(new PopupMenuListener() {

				@Override
				public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
					encodeTypeComboBox.removeAll();
					String item = (String) targetBox.getSelectItem();
					Datasource datasource = Application.getActiveApplication().getWorkspace().getDatasources().get(item);
					String datasetName = table.getValueAt(table.getSelectedRow(), COLUMN_INDEX_Dataset).toString();
					Dataset dataset = datasource.getDatasets().get(datasetName);
					ArrayList<String> encodeType = new ArrayList<String>();
					encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.NONE));
					if (dataset instanceof DatasetVector && (DatasetType.LINE == dataset.getType() || DatasetType.REGION == dataset.getType())) {
						encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.BYTE));
						encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.INT16));
						encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.INT24));
						encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.INT32));
					} else if (dataset instanceof DatasetGrid || dataset instanceof DatasetGridCollection) {
						encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.SGL));
						encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.LZW));
					} else if (dataset instanceof DatasetImage || dataset instanceof DatasetImageCollection) {
						encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.DCT));
						encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.LZW));
						encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.PNG));
						// TODO 组件有bug 获取不到 ColorSpaceType，先不处理
						// if(dataset instanceof DatasetImage && ColorSpaceType.RGB == ((DatasetImage)dataset).getColorSpace()){
						//
						// encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.COMPOUND));
						// }
					}
					encodeTypeComboBox.setModel(new DefaultComboBoxModel<String>(encodeType.toArray(new String[encodeType.size()])));

				}

				@Override
				public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
					// empty

				}

				@Override
				public void popupMenuCanceled(PopupMenuEvent e) {
					// empty
				}
			});
			TableColumn encodeTypeColumn = table.getColumnModel().getColumn(COLUMN_INDEX_EncodeType);
			TableCellEditor cellEditor = new DefaultCellEditor(encodeTypeComboBox);
			encodeTypeColumn.setCellEditor(cellEditor);

			// 字符集
			ArrayList<String> charsetes = new ArrayList<String>();
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.OEM));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.EASTEUROPE));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.THAI));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.RUSSIAN));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.BALTIC));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.ARABIC));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.HEBREW));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.VIETNAMESE));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.TURKISH));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.GREEK));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.CHINESEBIG5));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.JOHAB));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.HANGEUL));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.SHIFTJIS));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.MAC));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.SYMBOL));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.DEFAULT));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.ANSI));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.UTF8));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.UTF7));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.WINDOWS1252));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.KOREAN));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.UNICODE));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.CYRILLIC));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.XIA5));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.XIA5GERMAN));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.XIA5SWEDISH));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.XIA5NORWEGIAN));
			ComboBoxCellEditor charseteCellEditor = new ComboBoxCellEditor();
			final UIComboBox comboBoxCharsete = charseteCellEditor.getComboBox();
			comboBoxCharsete.setModel(new DefaultComboBoxModel<Object>(charsetes.toArray(new String[charsetes.size()])));
			comboBoxCharsete.addPopupMenuListener(new PopupMenuListener() {

				@Override
				public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
					comboBoxCharsete.removeAll();
					String item = (String) targetBox.getSelectItem();
					Datasource datasource = Application.getActiveApplication().getWorkspace().getDatasources().get(item);
					String datasetName = table.getValueAt(table.getSelectedRow(), COLUMN_INDEX_Dataset).toString();
					Dataset dataset = datasource.getDatasets().get(datasetName);
					ArrayList<String> charsetes = new ArrayList<String>();
					if (dataset instanceof DatasetVector) {
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.OEM));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.EASTEUROPE));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.THAI));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.RUSSIAN));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.BALTIC));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.ARABIC));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.HEBREW));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.VIETNAMESE));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.TURKISH));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.GREEK));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.CHINESEBIG5));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.JOHAB));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.HANGEUL));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.SHIFTJIS));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.MAC));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.SYMBOL));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.DEFAULT));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.ANSI));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.UTF8));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.UTF7));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.WINDOWS1252));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.KOREAN));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.UNICODE));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.CYRILLIC));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.XIA5));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.XIA5GERMAN));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.XIA5SWEDISH));
						charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.XIA5NORWEGIAN));
					} else {
						charsetes.add(DataEditorProperties.getString("String_UnSupport"));
					}
					comboBoxCharsete.setModel(new DefaultComboBoxModel<Object>(charsetes.toArray(new String[charsetes.size()])));
				}

				@Override
				public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
					// empty

				}

				@Override
				public void popupMenuCanceled(PopupMenuEvent e) {
					// empty

				}
			});
			TableColumn charseteColumn = table.getColumnModel().getColumn(COLUMN_INDEX_Charset);
			charseteColumn.setCellEditor(charseteCellEditor);

		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	/**
	 * 全选
	 * 
	 * @param e
	 */
	private void buttonSelectAll_Click() {
		try {
			ListSelectionModel selectionModel = this.table.getSelectionModel();
			selectionModel.addSelectionInterval(0, this.table.getRowCount() - 1);
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	/**
	 * 反选
	 * 
	 * @param e
	 */
	private void buttonSelectInvert_Click() {
		try {
			int[] temp = this.table.getSelectedRows();
			ArrayList<Integer> selectedRows = new ArrayList<Integer>();
			for (int index = 0; index < temp.length; index++) {
				selectedRows.add(temp[index]);
			}

			ListSelectionModel selectionModel = table.getSelectionModel();
			selectionModel.clearSelection();
			for (int index = 0; index < this.table.getRowCount(); index++) {
				if (!selectedRows.contains(index)) {
					selectionModel.addSelectionInterval(index, index);
				}
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	/**
	 * 删除
	 * 
	 * @param e
	 */
	private void buttonDelete_Click() {
		deleteSelectedRow();
		setButtonState();
	}

	/**
	 * 删除选择行的实现
	 */
	private void deleteSelectedRow() {
		try {
			// 删除后设置第0行被选中
			if (0 < table.getRowCount()) {
				int[] selectRows = table.getSelectedRows();
				DDLExportTableModel tableModel = (DDLExportTableModel) table.getModel();
				tableModel.removeRows(selectRows);
				table.updateUI();
				if (0 < table.getRowCount()) {
					table.setRowSelectionInterval(0, 0);
				}
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	/**
	 * 统一设置的简单实现
	 * 
	 * @param e
	 */
	private void buttonSetting_Click() {
		try {
			JDialogUnifiedSet unifiedSet = new JDialogUnifiedSet(this, true, table);
			unifiedSet.setVisible(true);
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	/**
	 * 复制的实现
	 */
	private void CopyDataset() {
		// 进度条实现
		FormProgressTotal formProgress = new FormProgressTotal();
		formProgress.doWork(new DatasetCopyCallable(table));
		if (this.chckbxAutoClose.isSelected()) {
			this.dispose();
		}
	}

	private void okButton_Click() {
		CopyDataset();
	}

	/**
	 * 取消
	 *
	 * @param e
	 */
	private void cancelButton_Click() {
		try {
			this.setVisible(false);
			this.dialogResult = DialogResult.CANCEL;
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}
}
