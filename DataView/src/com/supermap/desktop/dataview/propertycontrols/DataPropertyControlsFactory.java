package com.supermap.desktop.dataview.propertycontrols;

import java.util.ArrayList;

import javax.swing.JDialog;

import com.supermap.data.CursorType;
import com.supermap.data.Dataset;
import com.supermap.data.DatasetGrid;
import com.supermap.data.DatasetImage;
import com.supermap.data.DatasetVector;
import com.supermap.data.Datasource;
import com.supermap.data.EngineType;
import com.supermap.data.Recordset;
import com.supermap.data.Workspace;
import com.supermap.desktop.Application;
import com.supermap.desktop.controls.utilties.NodeDataTypeUtilties;
import com.supermap.desktop.ui.controls.NodeDataType;
import com.supermap.desktop.ui.controls.TreeNodeData;

public class DataPropertyControlsFactory {
	private WorkspacePropertyControl workspacePropertyControl;
	private DatasourcePropertyControl datasourcePropertyControl;
	private PrjCoordSysPropertyControl prjCoordSysPropertyControl;
	private DatasetPropertyControl datasetPropertyControl;
	private VectorPropertyControl vectorPropertyControl;
	private GridPropertyControl gridPropertyControl;
	private ImagePropertyControl imagePropertyControl;
	private RecordsetPropertyControl recordsetPropertyControl;

	private JDialog container;

	public DataPropertyControlsFactory(JDialog container) {
		this.container = container;
	}

	public AbstractPropertyControl[] createPropertyControl(TreeNodeData data) {
		ArrayList<AbstractPropertyControl> controls = new ArrayList<AbstractPropertyControl>();

		try {
			if (data != null) {
				if (data.getType() == NodeDataType.WORKSPACE) {
					controls.add(getWorkspacePropertyControl((Workspace) data.getData()));
				} else if (data.getType() == NodeDataType.DATASOURCE) {
					Datasource tempDatasource = (Datasource) data.getData();
					boolean covert = tempDatasource != null && !tempDatasource.isReadOnly();
					controls.add(getDatasourcePropertyControl((Datasource) data.getData()));
					controls.add(getPrjCoordSysPropertyControl(new DatasourcePrjCoordSysHandle((Datasource) data.getData()), covert));
				} else if (NodeDataTypeUtilties.isNodeDataset(data.getType())) {
					controls.add(getDatasetPropertyControl((Dataset) data.getData()));
					if (data.getType() == NodeDataType.DATASET_VECTOR) {
						controls.add(getVectorPropertyControl((DatasetVector) data.getData()));
						controls.add(getRecordsetPropertyControl((DatasetVector) data.getData()));
					} else if (data.getType() == NodeDataType.DATASET_GRID) {
						controls.add(getGridPropertyControl((DatasetGrid) data.getData()));
					} else if (data.getType() == NodeDataType.DATASET_IMAGE) {
						controls.add(getImagePropertyControl((DatasetImage) data.getData()));
					}
					boolean covert = !((Dataset) data.getData()).isReadOnly();
					controls.add(getPrjCoordSysPropertyControl(new DatasetPrjCoordSysHandle((Dataset) data.getData()), covert));
				}
			}
		} catch (Exception e) {
			Application.getActiveApplication().getOutput().output(e);
		}
		return controls.toArray(new AbstractPropertyControl[controls.size()]);
	}

	private WorkspacePropertyControl getWorkspacePropertyControl(Workspace workspace) {
		if (this.workspacePropertyControl == null) {
			this.workspacePropertyControl = new WorkspacePropertyControl(workspace, this.container);
		} else {
			this.workspacePropertyControl.setWorkspace(workspace);
		}

		return this.workspacePropertyControl;
	}

	private DatasourcePropertyControl getDatasourcePropertyControl(Datasource datasource) {
		if (this.datasourcePropertyControl == null) {
			this.datasourcePropertyControl = new DatasourcePropertyControl(datasource, this.container);
		} else {
			this.datasourcePropertyControl.setDatasource(datasource);
		}

		return this.datasourcePropertyControl;
	}

	private PrjCoordSysPropertyControl getPrjCoordSysPropertyControl(PrjCoordSysHandle prjHandle, boolean covertFlag) {
		if (this.prjCoordSysPropertyControl == null) {
			this.prjCoordSysPropertyControl = new PrjCoordSysPropertyControl(prjHandle, covertFlag, this.container);
		} else {
			this.prjCoordSysPropertyControl.setPrjCoordSys(prjHandle, covertFlag);
		}

		return this.prjCoordSysPropertyControl;
	}

	private DatasetPropertyControl getDatasetPropertyControl(Dataset dataset) {
		if (this.datasetPropertyControl == null) {
			this.datasetPropertyControl = new DatasetPropertyControl(dataset, this.container);
		} else {
			this.datasetPropertyControl.setDataset(dataset);
		}

		return this.datasetPropertyControl;
	}

	private VectorPropertyControl getVectorPropertyControl(DatasetVector datasetVector) {
		if (this.vectorPropertyControl == null) {
			this.vectorPropertyControl = new VectorPropertyControl(datasetVector, this.container);
		} else {
			this.vectorPropertyControl.setDatasetVector(datasetVector);
		}

		return this.vectorPropertyControl;
	}

	private GridPropertyControl getGridPropertyControl(DatasetGrid datasetGrid) {
		if (this.gridPropertyControl == null) {
			this.gridPropertyControl = new GridPropertyControl(datasetGrid, this.container);
		} else {
			this.gridPropertyControl.setDatasetGrid(datasetGrid);
		}

		return this.gridPropertyControl;
	}

	private ImagePropertyControl getImagePropertyControl(DatasetImage datasetImage) {
		if (this.imagePropertyControl == null) {
			this.imagePropertyControl = new ImagePropertyControl(datasetImage, this.container);
		} else {
			this.imagePropertyControl.setDatasetImage(datasetImage);
		}

		return this.imagePropertyControl;
	}

	private RecordsetPropertyControl getRecordsetPropertyControl(DatasetVector datasetVector) {
		if (this.recordsetPropertyControl == null) {
			this.recordsetPropertyControl = new RecordsetPropertyControl(datasetVector, this.container);
		} else {
			this.recordsetPropertyControl.setDatasetVector(datasetVector);
		}

		return this.recordsetPropertyControl;
	}
}
