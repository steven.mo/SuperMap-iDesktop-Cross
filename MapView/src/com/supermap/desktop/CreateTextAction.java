package com.supermap.desktop;

import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.border.Border;

import com.supermap.data.GeoText;
import com.supermap.data.Geometry;
import com.supermap.data.Point2D;
import com.supermap.data.TextPart;
import com.supermap.desktop.utilties.StringUtilties;
import com.supermap.ui.Action;
import com.supermap.ui.ActionChangedEvent;
import com.supermap.ui.ActionChangedListener;
import com.supermap.ui.MapControl;
import com.supermap.ui.TrackedEvent;
import com.supermap.ui.TrackedListener;

/**
 * 封装 MapControl 上文本绘制的操作
 * 
 * @author highsad
 *
 */
public class CreateTextAction {

	private static final double DEFAULT_FONT_HEIGHT = 20;

	private JTextField textFieldInput = new JTextField();
	private MapControl mapControl;
	private LayoutManager preLayout;
	private GeoText editingGeoText; // 用来记录每一次点击获取到的 GeoText，当鼠标在另一个位置点击再次出发 Tracked 的时候，将编辑的数据保存

	private TrackedListener trackedListener = new TrackedListener() {

		@Override
		public void tracked(TrackedEvent arg0) {
			mapControlTracked(arg0);

		}
	};
	private ActionChangedListener actionChangedListener = new ActionChangedListener() {

		@Override
		public void actionChanged(ActionChangedEvent arg0) {

			if (arg0.getOldAction() == Action.CREATETEXT) { // 结束文本对象绘制
				endAction();
			} else if (arg0.getNewAction() == Action.CREATETEXT) { // 开始文本对象绘制
				startAction();
			}
		}
	};
	private ActionListener actionListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			textFieldInputAction();
		}
	};

	public CreateTextAction() {
		this.textFieldInput.setBorder(null);
		this.textFieldInput.setSize(new Dimension(120, 23));
	}

	/**
	 * 开始文本绘制
	 */
	public void Start(MapControl mapControl) {
		if (this.mapControl != null && this.mapControl != mapControl) {
			endAction();
		}

		this.mapControl = mapControl;
		// 先添加监听，再设置 Action，才能在 ActionChanged 里处理
		this.mapControl.addActionChangedListener(this.actionChangedListener);
		this.mapControl.setAction(Action.CREATETEXT);
	}

	private void startAction() {
		this.preLayout = this.mapControl.getLayout();
		this.mapControl.setLayout(null);
		this.mapControl.add(this.textFieldInput);
		this.mapControl.addTrackedListener(this.trackedListener);
		this.textFieldInput.addActionListener(this.actionListener);
	}

	private void endAction() {
		this.mapControl.remove(this.textFieldInput);
		this.mapControl.setLayout(this.preLayout);
		this.mapControl.removeTrackedListener(this.trackedListener);
		this.mapControl.removeActionChangedListener(this.actionChangedListener);
		this.textFieldInput.removeActionListener(this.actionListener);
	}

	private void mapControlTracked(TrackedEvent e) {
		commitEditing();

		this.editingGeoText = (GeoText) e.getGeometry();
		this.editingGeoText.getTextStyle().setFontHeight(DEFAULT_FONT_HEIGHT);

		// 获取 GeoText 的位置，将文本编辑控件显示到那个位置
		Point2D centerPointMap = this.editingGeoText.getInnerPoint();
		Point inputLocation = this.mapControl.getMap().mapToPixel(centerPointMap);
		this.textFieldInput.setLocation(inputLocation);
		this.textFieldInput.setVisible(true);
		this.textFieldInput.requestFocus();
	}

	private void textFieldInputAction() {
		commitEditing();
	}

	private void commitEditing() {

		// 表明有待提交的编辑
		if (this.editingGeoText != null) {
			String text = this.textFieldInput.getText();
			if (!StringUtilties.isNullOrEmpty(text)) {
				TextPart textPart = new TextPart(text, this.editingGeoText.getPart(0).getAnchorPoint());
				this.editingGeoText.setPart(0, textPart);
				this.editingGeoText = null;
				this.mapControl.getMap().refresh();
			}
		}

		// Action结束之前，编辑提交之后，下一次编辑开始之前，隐藏编辑控件
		this.textFieldInput.setVisible(false);
	}
}
