package com.supermap.desktop.CtrlAction.Theme;

import javax.swing.JFrame;

import com.supermap.desktop.Application;
import com.supermap.desktop.Interface.IBaseItem;
import com.supermap.desktop.Interface.IForm;
import com.supermap.desktop.implement.CtrlAction;
import com.supermap.desktop.theme.CreateThemeContainer;

public class CtrlActionThemeModify extends CtrlAction{

	public CtrlActionThemeModify(IBaseItem caller, IForm formClass) {
		super(caller, formClass);
	}
	
	@Override
	public void run(){
		JFrame frame = (JFrame) Application.getActiveApplication().getMainFrame();
		CreateThemeContainer container = new CreateThemeContainer(frame, true);
		container.setVisible(true);
	}
	
	@Override
	public boolean enable(){
		return true;
	}
}
