package com.supermap.desktop;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.supermap.data.Datasource;
import com.supermap.data.DatasourceConnectionInfo;
import com.supermap.desktop.Interface.IBaseItem;
import com.supermap.desktop.Interface.ICtrlAction;
import com.supermap.desktop.Interface.IForm;
import com.supermap.desktop.implement.CtrlAction;

public class CtrlActionDatasourceOpenFile extends CtrlAction {

	public CtrlActionDatasourceOpenFile(IBaseItem caller, IForm formClass) {
		super(caller, formClass);
	}

	@Override
	public void run() {
		// TODO 没看到在哪用这个类
		try {
			JFileChooser fileChooser = new JFileChooser();
			FileNameExtensionFilter filter = new FileNameExtensionFilter("Datasource Files(udb)", "udb"); //$NON-NLS-2$ //$NON-NLS-3$

			fileChooser.addChoosableFileFilter(filter);
			fileChooser.setMultiSelectionEnabled(false);
			fileChooser.setDialogTitle("Open Datasource File");

			if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				// 判断工作空间是否修改了，但是没有 IsModify 属性 if (Application.getActiveApplication().getWorkspace().get)
				Application.getActiveApplication().getWorkspace().close();

				DatasourceConnectionInfo info = new DatasourceConnectionInfo(fileChooser.getSelectedFile().getAbsolutePath(), fileChooser.getSelectedFile()
						.getName(), "");

				Datasource datasource = Application.getActiveApplication().getWorkspace().getDatasources().open(info);
				String resultInfo = ""; //$NON-NLS-1$
				if (datasource != null) {
					resultInfo = "Open Workspace Succeed.";
				} else {
					resultInfo = "Open Workspace Failed.";
				}
				Application.getActiveApplication().getOutput().output(resultInfo);
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	@Override
	public boolean enable() {
		// TODO Auto-generated method stub
		return true;
	}

}
