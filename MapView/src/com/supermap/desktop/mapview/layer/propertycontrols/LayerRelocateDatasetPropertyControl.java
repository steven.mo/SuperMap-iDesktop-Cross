package com.supermap.desktop.mapview.layer.propertycontrols;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.TitledBorder;

import com.supermap.desktop.Application;
import com.supermap.desktop.DefaultValues;
import com.supermap.desktop.controls.ControlsProperties;
import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.mapview.layer.propertymodel.LayerPropertyModel;
import com.supermap.desktop.mapview.layer.propertymodel.LayerRelocateDatasetPropertyModel;
import com.supermap.desktop.properties.CoreProperties;
import com.supermap.desktop.ui.controls.comboBox.ComboBoxDataset;
import com.supermap.desktop.ui.controls.comboBox.ComboBoxDatasource;

public class LayerRelocateDatasetPropertyControl extends AbstractLayerPropertyControl implements ILayerProperty {

	private static final long serialVersionUID = 1L;

	private JLabel labelDatasource;
	private JLabel labelDataset;

	private ComboBoxDatasource comboBoxDatasource;
	private ComboBoxDataset comboBoxDataset;

	private ComboBoxItemListener comboBoxItemListener = new ComboBoxItemListener();

	public LayerRelocateDatasetPropertyControl() {
		// TODO
	}

	@Override
	public LayerRelocateDatasetPropertyModel getLayerPropertyModel() {
		return (LayerRelocateDatasetPropertyModel) super.getLayerPropertyModel();
	}

	@Override
	protected LayerRelocateDatasetPropertyModel getModifiedLayerPropertyModel() {
		return (LayerRelocateDatasetPropertyModel) super.getModifiedLayerPropertyModel();
	}

	@Override
	protected void initializeComponents() {
		this.setBorder(BorderFactory.createTitledBorder("RelocateDataset"));

		this.labelDatasource = new JLabel("Datasource:");
		this.labelDataset = new JLabel("Dataset");

		this.comboBoxDatasource = new ComboBoxDatasource();
		this.comboBoxDataset = new ComboBoxDataset();

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setAutoCreateContainerGaps(true);
		groupLayout.setAutoCreateGaps(true);
		this.setLayout(groupLayout);

		// @formatter:off
		groupLayout.setHorizontalGroup(groupLayout.createSequentialGroup()
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(labelDatasource, GroupLayout.PREFERRED_SIZE, DefaultValues.DEFAULT_LABEL_WIDTH, Short.MAX_VALUE)
						.addComponent(labelDataset, GroupLayout.PREFERRED_SIZE, DefaultValues.DEFAULT_LABEL_WIDTH, Short.MAX_VALUE))
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(comboBoxDatasource, GroupLayout.PREFERRED_SIZE, DefaultValues.DEFAULT_COMPONENT_WIDTH, Short.MAX_VALUE)
						.addComponent(comboBoxDataset, GroupLayout.PREFERRED_SIZE, DefaultValues.DEFAULT_COMPONENT_WIDTH, Short.MAX_VALUE)));
		
		groupLayout.setVerticalGroup(groupLayout.createSequentialGroup()
				.addGroup(groupLayout.createParallelGroup(Alignment.CENTER)
						.addComponent(labelDatasource)
						.addComponent(comboBoxDatasource, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createParallelGroup(Alignment.CENTER)
						.addComponent(labelDataset)
						.addComponent(comboBoxDataset, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)));
		// @formatter:on
	}

	@Override
	protected void initializeResources() {
		((TitledBorder) this.getBorder()).setTitle(MapViewProperties.getString("String_FormRelateDataset_Title"));
		this.labelDatasource.setText(ControlsProperties.getString("String_Label_ResultDatasource"));
		this.labelDataset.setText(ControlsProperties.getString("String_Label_ResultDataset"));
	}

	@Override
	protected void fillComponents() {
		if (getLayerPropertyModel() != null) {
			if (getLayerPropertyModel().getDataset() != null) {
				this.comboBoxDatasource.setSelectedDatasource(getLayerPropertyModel().getDataset().getDatasource());
				this.comboBoxDataset.setDatasource(getLayerPropertyModel().getDataset().getDatasource());
				this.comboBoxDataset.setDatasetTypes(getLayerPropertyModel().getDataset().getType());
				this.comboBoxDataset.setSelectedDataset(getLayerPropertyModel().getDataset());
			} else {
				this.comboBoxDataset.setDatasource(this.comboBoxDatasource.getSelectedDatasource());
				this.comboBoxDataset.setSelectedDataset(null);
			}
		}
	}

	@Override
	protected void registerEvents() {
		this.comboBoxDatasource.addItemListener(comboBoxItemListener);
		this.comboBoxDataset.addItemListener(comboBoxItemListener);
	}

	@Override
	protected void unregisterEvents() {
		this.comboBoxDatasource.removeItemListener(comboBoxItemListener);
		this.comboBoxDataset.removeItemListener(comboBoxItemListener);
	}

	private void comboBoxDatasourceSelectedItemChanged(ItemEvent e) {
		try {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				this.comboBoxDataset.setDatasource(this.comboBoxDatasource.getSelectedDatasource());
			}
		} catch (Exception e2) {
			Application.getActiveApplication().getOutput().output(e2);
		}
	}

	private void comboBoxDatasetSelectedItemChanged(ItemEvent e) {
		try {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				getModifiedLayerPropertyModel().setDataset(this.comboBoxDataset.getSelectedDataset());
				checkChanged();
			}
		} catch (Exception e2) {
			Application.getActiveApplication().getOutput().output(e2);
		}
	}

	private class ComboBoxItemListener implements ItemListener {

		@Override
		public void itemStateChanged(ItemEvent e) {
			if (e.getSource() == comboBoxDatasource) {
				comboBoxDatasourceSelectedItemChanged(e);
			} else if (e.getSource() == comboBoxDataset) {
				comboBoxDatasetSelectedItemChanged(e);
			}
		}
	}

	@Override
	protected void setControlEnabled(String propertyName, boolean enabled) {
		if (propertyName.equals(LayerRelocateDatasetPropertyModel.DATASET)) {
			this.comboBoxDataset.setEnabled(enabled);
			this.comboBoxDatasource.setEnabled(enabled);
		}
	}
}
