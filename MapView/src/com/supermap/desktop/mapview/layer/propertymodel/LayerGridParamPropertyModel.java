package com.supermap.desktop.mapview.layer.propertymodel;

import java.awt.Color;

import com.supermap.data.Colors;
import com.supermap.mapping.Layer;
import com.supermap.mapping.LayerSettingGrid;
import com.supermap.mapping.Map;

public class LayerGridParamPropertyModel extends LayerPropertyModel {

	public static final String BRIGHTNESS = "brightness";
	public static final String CONTRAST = "contrast";
	public static final String COLORS = "colors";
	public static final String SPECIAL_VALUE = "specialValue";
	public static final String SPECIAL_VALUE_COLOR = "specialValueColor";
	public static final String IS_SPECIAL_VALUE_TRANSPARENT = "isSpecialValueTransparent";

	private Integer brightness = 0;
	private Integer contrast = 0;
	private Colors colors;
	private Double specialValue = 0.0;
	private Color specialValueColor = Color.WHITE;
	private Boolean isSpecialValueTransparent = false;

	public LayerGridParamPropertyModel() {

	}

	public LayerGridParamPropertyModel(Layer[] layers, Map map) {
		super(layers, map);
		initializeProperties(layers);
	}

	public Integer getBrightness() {
		return brightness;
	}

	public void setBrightness(Integer brightness) {
		this.brightness = brightness;
	}

	public Integer getContrast() {
		return contrast;
	}

	public void setContrast(Integer contrast) {
		this.contrast = contrast;
	}

	public Colors getColors() {
		return colors;
	}

	public void setColors(Colors colors) {
		this.colors = colors;
	}

	public Double getSpecialValue() {
		return specialValue;
	}

	public void setSpecialValue(Double specialValue) {
		this.specialValue = specialValue;
	}

	public Color getSpecialValueColor() {
		return specialValueColor;
	}

	public void setSpecialValueColor(Color specialValueColor) {
		this.specialValueColor = specialValueColor;
	}

	public Boolean isSpecialValueTransparent() {
		return isSpecialValueTransparent;
	}

	public void setSpecialValueTransparent(Boolean isSpecialValueTransparent) {
		this.isSpecialValueTransparent = isSpecialValueTransparent;
	}

	@Override
	public void setProperties(LayerPropertyModel model) {
		LayerGridParamPropertyModel gridParamPropertyModel = (LayerGridParamPropertyModel) model;

		if (gridParamPropertyModel != null) {
			this.brightness = gridParamPropertyModel.getBrightness();
			this.contrast = gridParamPropertyModel.getContrast();
			this.colors = gridParamPropertyModel.getColors();
			this.specialValue = gridParamPropertyModel.getSpecialValue();
			this.specialValueColor = gridParamPropertyModel.getSpecialValueColor();
			this.isSpecialValueTransparent = gridParamPropertyModel.isSpecialValueTransparent();
		}
	}

	@Override
	public boolean equals(LayerPropertyModel model) {
		LayerGridParamPropertyModel gridParamPropertyModel = (LayerGridParamPropertyModel) model;

		return gridParamPropertyModel != null && this.brightness == gridParamPropertyModel.getBrightness()
				&& this.contrast == gridParamPropertyModel.getContrast() && this.colors == gridParamPropertyModel.getColors()
				&& Double.compare(this.specialValue, gridParamPropertyModel.getSpecialValue()) == 0
				&& this.specialValueColor == gridParamPropertyModel.getSpecialValueColor()
				&& this.isSpecialValueTransparent == gridParamPropertyModel.isSpecialValueTransparent();
	}

	@Override
	protected void apply(Layer layer) {
		if (layer != null && layer.getAdditionalSetting() instanceof LayerSettingGrid) {
			LayerSettingGrid setting = (LayerSettingGrid) layer.getAdditionalSetting();

			if (this.propertyEnabled.get(BRIGHTNESS) && this.brightness != null) {
				setting.setBrightness(this.brightness);
			}

			if (this.propertyEnabled.get(CONTRAST) && this.contrast != null) {
				setting.setContrast(this.contrast);
			}

			if (this.propertyEnabled.get(COLORS) && this.colors != null) {
				setting.setColorTable(this.colors);
			}

			if (this.propertyEnabled.get(SPECIAL_VALUE) && this.specialValue != null) {
				setting.setSpecialValue(this.specialValue);
			}

			if (this.propertyEnabled.get(SPECIAL_VALUE_COLOR) && this.specialValueColor != null) {
				setting.setSpecialValueColor(this.specialValueColor);
			}

			if (this.propertyEnabled.get(IS_SPECIAL_VALUE_TRANSPARENT) && this.isSpecialValueTransparent != null) {
				setting.setSpecialValueTransparent(this.isSpecialValueTransparent);
			}
		}
	}

	private void initializeProperties(Layer[] layers) {
		resetProperties();
		initializeEnabledMap();

		for (Layer layer : layers) {
			if (layer == null) {
				break;
			}

			LayerSettingGrid setting = (LayerSettingGrid) layer.getAdditionalSetting();
			this.brightness = ComplexPropertyUtilties.union(this.brightness, setting.getBrightness());
			this.contrast = ComplexPropertyUtilties.union(this.contrast, setting.getContrast());
			this.colors = ComplexPropertyUtilties.union(this.colors, setting.getColorTable());
			this.specialValue = ComplexPropertyUtilties.union(this.specialValue, setting.getSpecialValue());
			this.specialValueColor = ComplexPropertyUtilties.union(this.specialValueColor, setting.getSpecialValueColor());
			this.isSpecialValueTransparent = ComplexPropertyUtilties.union(this.isSpecialValueTransparent, setting.isSpecialValueTransparent());
		}
	}

	// 使用指定的 Layer 初始化值
	private void resetProperties() {
		this.brightness = 0;
		this.contrast = 0;
		this.specialValue = 0.0;
		this.specialValueColor = Color.WHITE;
		this.isSpecialValueTransparent = false;

		if (getLayers() != null && getLayers().length > 0) {
			this.brightness = ((LayerSettingGrid) getLayers()[0].getAdditionalSetting()).getBrightness();
			this.contrast = ((LayerSettingGrid) getLayers()[0].getAdditionalSetting()).getContrast();
			this.specialValue = ((LayerSettingGrid) getLayers()[0].getAdditionalSetting()).getSpecialValue();
			this.specialValueColor = ((LayerSettingGrid) getLayers()[0].getAdditionalSetting()).getSpecialValueColor();
			this.isSpecialValueTransparent = ((LayerSettingGrid) getLayers()[0].getAdditionalSetting()).isSpecialValueTransparent();
			this.colors = ((LayerSettingGrid) getLayers()[0].getAdditionalSetting()).getColorTable();
		}
	}

	private void initializeEnabledMap() {
		this.propertyEnabled.put(BRIGHTNESS, true);
		this.propertyEnabled.put(CONTRAST, true);
		this.propertyEnabled.put(COLORS, true);
		this.propertyEnabled.put(SPECIAL_VALUE, true);
		this.propertyEnabled.put(SPECIAL_VALUE_COLOR, true);
		this.propertyEnabled.put(IS_SPECIAL_VALUE_TRANSPARENT, true);
	}
}
