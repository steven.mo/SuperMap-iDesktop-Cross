package com.supermap.desktop.theme;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.supermap.desktop.Application;
import com.supermap.desktop.Interface.IFormMap;
import com.supermap.desktop.ui.UICommonToolkit;
import com.supermap.desktop.ui.controls.SmDialog;
import com.supermap.mapping.Layer;

public class CreateThemeContainer extends SmDialog {

	/**
	 * 标签专题图入口容器
	 */
	private static final long serialVersionUID = 1L;

	public CreateThemeContainer(JFrame owner, boolean modal) {
		super(owner, modal);
		initComponents();
	}

	private void initComponents() {
		setResizable(false);
		setLocation(600, 350);
		setSize(580, 450);
		add(getBasicThemePanel());
		this.addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				UICommonToolkit.getLayersManager().getLayersTree().reload();
			}

		});
	}

	private JPanel getBasicThemePanel() {
		IFormMap formMap = (IFormMap) Application.getActiveApplication().getActiveForm();
		Layer layer = null;
		if (0 < formMap.getActiveLayers().length) {
			layer = formMap.getActiveLayers()[0];
		} else {
			layer = formMap.getMapControl().getMap().getLayers().get(0);
		}
		ThemeGuide themeGuide = new ThemeGuide(layer, this);
		ThemeMainPanel result = new ThemeMainPanel(layer, themeGuide, this);
		return result;
	};
}
