package com.supermap.desktop.theme;

import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.EventObject;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

import com.supermap.data.GeoText;
import com.supermap.data.TextStyle;
import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.ui.UICommonToolkit;
import com.supermap.desktop.ui.controls.DialogResult;
import com.supermap.desktop.ui.controls.TextStyleDialog;
import com.supermap.mapping.Map;

public class JPanelCellEditorForLabel extends JPanel implements TableCellEditor, ActionListener, MouseListener {
	private static final long serialVersionUID = 1L;

	int row;

	int col;

	private JLabel jLabel;

	private JCheckBox jCheckBox;

	private JTable jTable;

	private transient TextStyleDialog textStyleDialog;

	public JPanelCellEditorForLabel() {
		super();
		this.setLayout(new GridLayout(1, 1));
		this.setBorder(BorderFactory.createEmptyBorder(0, 0, 2, 2));
		jLabel = new JLabel();
		jCheckBox = new JCheckBox("", true);
		jCheckBox.setFocusable(false);// 此处使它失去焦点解决来解决它点击一次不能得到焦点的问题
		jCheckBox.addActionListener(this);
		jLabel.addMouseListener(this);
		this.add(jCheckBox);
		this.add(jLabel);

	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		ImageIcon icon = new ImageIcon();
		BufferedImage bi = new BufferedImage(30, 30, BufferedImage.TYPE_INT_ARGB);
		Graphics2D gg = (Graphics2D) bi.getGraphics();
		gg.setColor(((UniqueValue) table.getValueAt(row, column)).getColor());
		gg.fillRect(5, 8, 30, 30);
		icon.setImage(bi);
		jLabel.setIcon(icon);
		jCheckBox.setSelected(((UniqueValue) table.getValueAt(row, column)).isFlag());
		jTable = table;
		this.row = row;
		col = column;
		return this;
	}

	@Override
	public Object getCellEditorValue() {
		return (UniqueValue) jTable.getValueAt(row, col);
	}

	@Override
	public boolean isCellEditable(EventObject anEvent) {
		return true;
	}

	@Override
	public boolean shouldSelectCell(EventObject anEvent) {
		return true;
	}

	@Override
	public boolean stopCellEditing() {
		return true;
	}

	@Override
	public void cancelCellEditing() {
		// TODO something
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Boolean flag = ((JCheckBox) e.getSource()).isSelected();
		((UniqueValue) jTable.getValueAt(row, col)).setFlag(flag);
		JPanelCellEditorForLabel.this.stopCellEditing();
		firePropertyChange("Check", null, flag);
	}

	@Override
	public void addCellEditorListener(CellEditorListener l) {
		// TODO something
	}

	@Override
	public void removeCellEditorListener(CellEditorListener l) {
		// TODO something
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getButton() == 1 && e.getClickCount() == 2) {
			int selectIndex = jTable.getSelectedRow();
			if (selectIndex > -1) {
				String name = jTable.getColumnName(0);
				int width = jTable.getColumn(name).getWidth();
				int height = jTable.getTableHeader().getHeight();
				int x = jTable.getLocationOnScreen().x + width;
				int y = jTable.getLocationOnScreen().y - height;
				int dialogX = x;
				int dialogY = y;
				textStyleDialog = new TextStyleDialog();
				textStyleDialog.setLocation(dialogX, dialogY);

				textStyleDialog.setGeoText(new GeoText());
				textStyleDialog.setMapObject(new Map());
				DialogResult dialogResult = textStyleDialog.showDialog();
				if (dialogResult.equals(DialogResult.OK)) {
					TextStyle textStyle = textStyleDialog.getTextStyle();
					((UniqueValue) jTable.getValueAt(row, col)).setColor(textStyle.getForeColor());
					getTableCellEditorComponent(jTable, null, true, row, col);
					firePropertyChange("Color", null, textStyle);
				}

			} else {
				UICommonToolkit.showMessageDialog(MapViewProperties.getString("String_Warning_NeedToSetTextStyle"));
			}

		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO something
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO something
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO something
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO something
	}
}
