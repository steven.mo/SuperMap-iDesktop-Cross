package com.supermap.desktop.theme;

import java.util.Vector;

import javax.swing.JPanel;

import com.supermap.mapping.Theme;
/**
 * 实现了专题图监听器的Jpanel
 * @author zhaosy
 *
 */
 abstract class ThemeChangePanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Theme theme;
	transient static Vector<ThemeChangedListener> themeChangedListeners;
	
	public synchronized void addThemeChangedListener(ThemeChangedListener l) {
		if (themeChangedListeners == null) {
			themeChangedListeners = new Vector<ThemeChangedListener>();
		}
		if (!themeChangedListeners.contains(l)) {
			themeChangedListeners.add(l);
		}
	}

	public synchronized void removeThemeChangedListener(ThemeChangedListener l) {
		if (themeChangedListeners != null && themeChangedListeners.contains(l)) {
			themeChangedListeners.remove(l);
		}
	}

	protected static void fireThemeChanged(ThemeChangedEvent event) {
		if (themeChangedListeners != null) {
			theme=	event.getTheme();
			Vector<ThemeChangedListener> listeners = themeChangedListeners;
			int count = listeners.size();
			for (int i = 0; i < count; i++) {
				listeners.elementAt(i).themeHandleChanged(event);
			}
		}
	}
	/**
	 * 设置Theme
	 * @param theme
	 */
    protected abstract void setTheme(Theme theme);
  /**
    * 返回Theme
    * @return
  */
    protected Theme getTheme(){
    	return theme;
    }
}
