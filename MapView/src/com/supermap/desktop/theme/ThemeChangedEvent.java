package com.supermap.desktop.theme;

import java.util.EventObject;

import com.supermap.mapping.Theme;
import com.supermap.mapping.ThemeLabel;
import com.supermap.mapping.ThemeRange;

/**
 * 专题图改变事件
 * 
 * @author zhaosy
 *
 */
class ThemeChangedEvent extends EventObject {
	private transient Theme theme;

	public Theme getTheme() {
		return theme;
	}

	public ThemeChangedEvent(Object source, ThemeLabel newTheme, ThemeLabel oldThemelabel) {
		super(source);
		resetThemeLableStyle(newTheme, oldThemelabel);
		theme = newTheme;
	}

	public ThemeChangedEvent(Object source, ThemeRange newTheme, ThemeRange oldThemeRange) {
		super(source);
		resetThemeRangeStyle(newTheme, oldThemeRange);
		theme = newTheme;

	}

	public ThemeChangedEvent(Object source, Theme theme) {
		super(source);

		this.theme = theme;
	}

	private void resetThemeRangeStyle(ThemeRange newTheme, ThemeRange oldThemeRange) {
		newTheme.setOffsetFixed(oldThemeRange.isOffsetFixed());
		newTheme.setOffsetX(oldThemeRange.getOffsetX());
		newTheme.setOffsetX(oldThemeRange.getOffsetY());
		newTheme.setPrecision(oldThemeRange.getPrecision());
		newTheme.setRangeExpression(oldThemeRange.getRangeExpression());
	}

	/**
	 * 当专题图执行makeDefault后调用此方法把以前的Style重新设置进去
	 * 
	 * @param newTheme
	 * @param oldTheme
	 */
	private void resetThemeLableStyle(ThemeLabel newTheme, ThemeLabel oldTheme) {
		newTheme.setAllDirectionsOverlapedAvoided(oldTheme.isAllDirectionsOverlapedAvoided());
		newTheme.setAlongLine(oldTheme.isAlongLine());
		newTheme.setAlongLineDirection(oldTheme.getAlongLineDirection());
		newTheme.setAlongLineSpaceRatio(oldTheme.getAlongLineSpaceRatio());
		newTheme.setAngleFixed(oldTheme.isAngleFixed());
		newTheme.setBackShape(oldTheme.getBackShape());
		newTheme.setBackStyle(oldTheme.getBackStyle());
		newTheme.setFlowEnabled(oldTheme.isFlowEnabled());
		newTheme.setLabelExpression(oldTheme.getLabelExpression());
		newTheme.setLabelRepeatInterval(oldTheme.getLabelRepeatInterval());
		newTheme.setLabels(oldTheme.getLabels());
		newTheme.setLeaderLineDisplayed(oldTheme.isLeaderLineDisplayed());
		newTheme.setLeaderLineStyle(oldTheme.getLeaderLineStyle());
		newTheme.setMaxTextHeight(oldTheme.getMaxTextHeight());
		newTheme.setMaxTextWidth(oldTheme.getMaxTextWidth());
		newTheme.setMinTextHeight(oldTheme.getMinTextHeight());
		newTheme.setMinTextWidth(oldTheme.getMinTextWidth());
		newTheme.setNumericPrecision(oldTheme.getNumericPrecision());
		newTheme.setOffsetFixed(oldTheme.isOffsetFixed());
		newTheme.setOffsetX(oldTheme.getOffsetX());
		newTheme.setOffsetY(oldTheme.getOffsetY());
		newTheme.setOverlapAvoided(oldTheme.isOverlapAvoided());
		newTheme.setOverLengthMode(oldTheme.getOverLengthMode());
		newTheme.setRangeExpression(oldTheme.getRangeExpression());
		newTheme.setRepeatedLabelAvoided(oldTheme.isRepeatedLabelAvoided());
		newTheme.setRepeatIntervalFixed(oldTheme.isRepeatIntervalFixed());
		newTheme.setSmallGeometryLabeled(oldTheme.isSmallGeometryLabeled());
		newTheme.setTextExtentInflation(oldTheme.getTextExtentInflation());
		newTheme.setUniformMixedSytle(oldTheme.getUniformMixedSytle());
		newTheme.setUniformStyle(oldTheme.getUniformStyle());
	}
}
