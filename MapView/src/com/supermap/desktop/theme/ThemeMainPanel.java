package com.supermap.desktop.theme;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
//import java.io.File;
//import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
//import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.parsers.ParserConfigurationException;

//import org.w3c.dom.Document;
//import org.xml.sax.SAXException;

import com.supermap.data.DatasetVector;
import com.supermap.data.Workspace;
//import com.supermap.desktop.mapview.MapViewActivator;
import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.properties.CommonProperties;
//import com.supermap.desktop.ui.controls.DialogResult;
import com.supermap.desktop.ui.controls.InternalImageIconFactory;
import com.supermap.mapping.Layer;
//import com.supermap.mapping.Theme;
import com.supermap.mapping.ThemeType;
import com.supermap.ui.MapControl;

/**
 * @author WangDY
 */
class ThemeMainPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	// 整体布局面板
	private JTree jTree;
	private JTextArea jTextArea;
	private JPanel jPanelDown;
	private JPanel jPanelMain;
	private JPanel jPanelLefet;
	private JPanel jPanelRightUp;
	private JPanel jPanelRightCenter;

	private JScrollPane jPaneScroll;

	// "上一步","下一步","取消","确定" 按钮
	private JButton jButtonNext;
	private JButton jButtonOk;

	// 专题图类树上的节点
	private DefaultMutableTreeNode uniqueNode;
	private DefaultMutableTreeNode rangedNode;
	private DefaultMutableTreeNode labelNode;
	// 标签专题图风格
	private ThemeLabelIcon uniqueStyelLabel;
	private ThemeLabelIcon rangeStyleLabel;
	private ThemeLabelIcon complicatedStyleLabel;
	private ThemeLabelIcon matrixStyleLabel;

	// 标签专题图的四种风格
	private DefaultMutableTreeNode uniqueStyleOfLabelTheme;
	private DefaultMutableTreeNode rangeStyleOfLabelTheme;

	// 专题图示例中保存的各个专题图类型的示例图片
	private ThemeLabelIcon uniqueThemeLabel;
	private ThemeLabelIcon rangeThemeLabel;
	private ThemeLabelIcon graphThemeLabel;
	private ThemeLabelIcon graduatedThemeLabel;
	private ThemeLabelIcon dotDensityLabel;
	private ThemeLabelIcon selfDefinitionLabel;

	private ThemeType currentThemeType = ThemeType.UNIQUE;
	private int currentLabelStyle = -1;
	private TreePath recordPath;

	// private Workspace m_workSpace = null;
	// private MapControl m_mapControl = null;
	private DatasetVector dataset;

	private TitledBorder titledBorder;
	private BasicThemePanel basicThemePanel;

	private boolean isMouseClicked = true;

	private ThemeMainPanel jPanelCurrent;
	private JPanel parent;

	private ThemeType recordThemeType;
	private int recordLabelStyle;
	private Layer layer;

	// 专题图模板
	// private DefaultMutableTreeNode m_themeModule;
	private StringBuilder title;
	private ThemeGuide m_themeGuide;
	// private Theme m_theme;
	// private DialogResult m_dialogResult;

	private final int defualt_theme_style = -1;
	private CreateThemeContainer container;
	private static final int uniformItem = 0;
	private static final int rangeItem = 1;
	private static final int mixedItem = 2;
	private static final int advanceItem = 3;
	// 快速查找当前是哪个专题图
	private static Map<ThemeType, String> mapOfThemeType = new HashMap<ThemeType, String>();
	private static Map<Integer, String> subMapOfThemeLabelStyle = new HashMap<Integer, String>();
	static {
		mapOfThemeType.put(ThemeType.UNIQUE, MapViewProperties.getString("String_Theme_Unique"));
		mapOfThemeType.put(ThemeType.CUSTOM, MapViewProperties.getString("String_ThemeCustom"));
		mapOfThemeType.put(ThemeType.RANGE, MapViewProperties.getString("String_Theme_Range"));
		mapOfThemeType.put(ThemeType.DOTDENSITY, MapViewProperties.getString("String_Theme_DotDensity"));
		mapOfThemeType.put(ThemeType.GRADUATEDSYMBOL, MapViewProperties.getString("String_Theme_GraduatedSymbol"));
		mapOfThemeType.put(ThemeType.GRAPH, MapViewProperties.getString("String_Theme_Graph"));
		mapOfThemeType.put(ThemeType.LABEL, MapViewProperties.getString("String_ThemeLabel"));

		subMapOfThemeLabelStyle.put(uniformItem, MapViewProperties.getString("String_ThemeLabelUniformItem"));
		subMapOfThemeLabelStyle.put(rangeItem, MapViewProperties.getString("String_ThemeLabelRangeItem"));
		subMapOfThemeLabelStyle.put(mixedItem, MapViewProperties.getString("String_ThemeLabelMixedItem"));
		subMapOfThemeLabelStyle.put(advanceItem, MapViewProperties.getString("String_ThemeLabelAdvanceItem"));

	}

	/**
	 * 初始化基本面板
	 * 
	 * @param layer
	 * @param guide
	 */
	public ThemeMainPanel(Layer layer, ThemeGuide guide, CreateThemeContainer container) {
		super();
		this.container = container;
		m_themeGuide = guide;
		this.layer = layer;
		dataset = (DatasetVector) layer.getDataset();
		jPanelCurrent = this;
		title = new StringBuilder();
		setLayout(null);
		initional();
		setSize(540, 425);
	}

	/**
	 * 初始化
	 */
	private void initional() {
		if (layer.getTheme() != null) {
			basicThemePanel = new BasicThemePanel(layer, m_themeGuide, container);
			basicThemePanel.setBounds(0, 0, 540, 435);
			add(basicThemePanel);
		} else {
			getMainPanel();
			getCenterPanel();
			getLeftPanel();
			getRightUpPanel();
			getDownPanel();
		}
	}

	BasicThemePanel getBasicPanel() {
		return basicThemePanel;
	}

	private JPanel getCenterPanel() {
		if (jPanelRightCenter == null) {
			jPanelRightCenter = new JPanel();
			titledBorder = new TitledBorder(new LineBorder(Color.LIGHT_GRAY,
					1, false), MapViewProperties.getString("String_UniqueTheme_Example"), TitledBorder.DEFAULT_JUSTIFICATION,
					TitledBorder.DEFAULT_POSITION, null, null);
			jPanelRightCenter.setBorder(titledBorder);
			jPanelRightCenter.setLayout(new GridLayout(1, 0));
			jPanelRightCenter.setBounds(170, 80, 345, 287);
			getMainPanel().add(jPanelRightCenter);
		}
		return jPanelRightCenter;
	}

	private JPanel getRightUpPanel() {
		if (jPanelRightUp == null) {
			jPanelRightUp = new JPanel();
			jPanelRightUp.setBorder(new TitledBorder(new LineBorder(
					Color.LIGHT_GRAY, 1, false), MapViewProperties.getString("String_Theme_Explain"),
					TitledBorder.DEFAULT_JUSTIFICATION,
					TitledBorder.DEFAULT_POSITION, null, null));
			jPanelRightUp.setLayout(null);
			jPanelRightUp.setBounds(170, 0, 345, 80);

			jTextArea = new JTextArea();
			jTextArea.setLineWrap(true);
			jTextArea.setFocusable(false);
			// 默认为单值专题图的功能说明
			jTextArea.append(MapViewProperties.getString("String_UniqueTheme_Function"));

			// 当只有标签专题图统一风格和分段风格时，默认相关信息为统一风格的
//			styleHandleForClick(0, MapViewProperties.getString("String_UniformTheme_Explain"), MapViewProperties.getString("String_Theme_UniformExaple"),
//					uniqueStyelLabel,
//					InternalImageIconFactory.THEMEGUIDE_PREVIEW_UNIFORM);

			jTextArea.setBackground(getBackground());
			jTextArea.setEditable(false);
			jTextArea.setBounds(10, 20, 325, 50);

			jPanelRightUp.add(jTextArea);
			getMainPanel().add(jPanelRightUp);
		}
		return jPanelRightCenter;
	}

	private JPanel getMainPanel() {
		if (jPanelMain == null) {
			jPanelMain = new JPanel();
			jPanelMain.setLayout(null);
			jPanelMain.setBounds(5, 0, 520, 365);
			add(jPanelMain);
		}
		return jPanelMain;
	}

	private JPanel getLeftPanel() {
		if (jPanelLefet == null) {
			jPanelLefet = new JPanel();
			jPanelLefet.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, false));
			jPanelLefet.setLayout(null);
			jPanelLefet.setBounds(0, 8, 165, 357);

			// 专题图类型树
			DefaultMutableTreeNode root = new DefaultMutableTreeNode("root");
			getChildNode(root);

			jTree = new JTree(root);
			jTree.setRootVisible(false);
			jTree.setBounds(15, 8, 137, 343);
			jTree.setBackground(new Color(238, 238, 238));
			jTree.getSelectionModel().setSelectionMode(
					TreeSelectionModel.SINGLE_TREE_SELECTION);
			// 默认标签专题图为打开状态，该statement只是暂时的
			jTree.expandRow(0);

			// 默认m_recordPath为单值
			// m_recordPath = m_tree.getPathForRow(0);

			// 当只有标签专题图的两个风格时，默认选中统一风格
			jTree.setSelectionRow(0);

			// 当只有标签专题图的两个风格时，m_recordPath默认为统一的
			recordPath = jTree.getPathForRow(1);

			jTree.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					isMouseClicked = false;
				}
			});

			jTree.setCellRenderer(new TreeCellRenderImp());
			jPaneScroll = new JScrollPane(
					JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
					JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			jPaneScroll.setViewportView(jTree);
			jPaneScroll.setBounds(15, 8, 137, 343);
			jPaneScroll.setBorder(null);
			addListenersToTree(jTree);
			jPanelLefet.add(jPaneScroll);

			// 默认选中单值专题图
			// m_tree.setSelectionRow(0);

			// 专题图预览
			uniqueThemeLabel = new ThemeLabelIcon(
					InternalImageIconFactory.THEMEGUIDE_PREVIEW_UNIQUE,
					new Dimension(jPanelRightCenter.getWidth(),
							jPanelRightCenter.getHeight()));
			jPanelRightCenter.add(uniqueThemeLabel);

			getMainPanel().add(jPanelLefet);
		}
		return jPanelLefet;
	}

	private void addListenersToTree(final JTree tree) {
		tree.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				// TODO: FileNameExtensionFilter 1.6 类型不能以源码的方式使用，故先注释起来
				// 当前进行鼠标点击事件
				isMouseClicked = true;

				TreePath path = tree.getPathForLocation(e.getX(), e.getY());
				if (path == null)
					return;
				// 打开标签模板
				// if (e.getClickCount() == 1
				// && path.getLastPathComponent().toString().equals(
				// MapViewProperties.getString("String_ImportTheme"))) {
				// JFileChooser chooser = new JFileChooser();
				// FileNameExtensionFilter filter = new FileNameExtensionFilter(
				// "XML", "XML");
				// chooser.setFileFilter(filter);
				// int returnVal = chooser.showOpenDialog(m_themeGuide);
				// if (returnVal == JFileChooser.APPROVE_OPTION) {
				// ThemeLabel theme = new ThemeLabel();
				// StringBuilder stringBuilder = new StringBuilder();
				// String string = null;
				// BufferedReader reader = null;
				// try {
				// reader = new BufferedReader(new
				// FileReader(chooser.getSelectedFile()));
				// string = reader.readLine();
				// while (!"</sml:Theme>".equals(string)) {
				// stringBuilder.append(string);
				// string = reader.readLine();
				// }
				// } catch (Exception e1) {
				// e1.printStackTrace();
				// } finally {
				// if (reader != null) {
				// try {
				// reader.close();
				// } catch (IOException exp) {
				// exp.printStackTrace();
				// }
				//
				// }
				// }
				// stringBuilder.append("</sml:Theme>");
				// boolean isTrue = theme.fromXML(stringBuilder.toString());
				// if (isTrue) {
				//
				// if (m_basicThemePanel != null) {
				//
				// m_basicThemePanel.disposeAll();
				// m_basicThemePanel = null;
				// }
				// m_currentThemeType = theme.getType();
				// m_dataset = (DatasetVector) m_layer.getDataset();
				// if (theme.getLabels() != null) {
				// m_currentLabelStyle = 3;
				// } else if (theme.getUniformMixedSytle() != null) {
				// m_currentLabelStyle = 2;
				// } else if (theme.getCount() > 0) {
				// m_currentLabelStyle = 1;
				// } else {
				// m_currentLabelStyle = 0;
				// }
				// // m_basicThemePanel = new BasicThemePanel(m_layer,
				// // theme, m_currentPanel);
				// m_basicThemePanel = new BasicThemePanel(
				// m_currentThemeType, m_currentLabelStyle,
				// m_dataset, m_currentPanel);
				// m_parent = (JPanel) ThemeMainPanel.this.getParent();
				// m_parent.remove(ThemeMainPanel.this);
				// m_parent.add(m_basicThemePanel);
				// m_parent.revalidate();
				// m_parent.repaint();
				// } else {
				// UICommonToolkit.showMessageDialog(MapViewProperties.getString("String_ErrorModel"));
				// }
				// }
				// // Theme theme=new Theme();
				// // dialog.getFile();
				//
				// m_basicThemePanel.disposeAll();
				// m_basicThemePanel = null;
				// m_basicThemePanel = new BasicThemePanel(
				// m_currentThemeType, m_currentLabelStyle,
				// m_dataset, m_currentPanel);
				// m_parent.add(m_basicThemePanel);
				//
				// }

				// expand or collapse 标签专题图
				if (e.getClickCount() >= 1) {
					if (path.getLastPathComponent().toString().equals(MapViewProperties.getString("String_ThemeLabel"))) {
						if (tree.isExpanded(path)) {
							tree.setSelectionPath(recordPath);
							return;
						} else {
							// 点击标签专题图类型时,判断曾经点击过的是标签子风格 or 其他专题图类型
							// 其他专题图
							if (!recordPath.getParentPath()
									.getLastPathComponent().toString().equals(
											MapViewProperties.getString("String_ThemeLabel"))) {
								path = path.pathByAddingChild(tree.getModel()
										.getChild(path.getLastPathComponent(),
												0));
								recordPath = path;
							}
							tree.setSelectionPath(recordPath);
							tree.expandPath(recordPath);
						}
					} else {
						recordPath = path;
					}
				}
				String selectedTheme = recordPath.getLastPathComponent().toString();
				refreshView(selectedTheme);
			}

		});
	}

	/**
	 * 处理选中不同专题图类型的情况
	 * 
	 * @param selectedTheme themeName
	 */
	private void refreshView(String selectedTheme) {
		if (MapViewProperties.getString("String_Theme_Unique").equals(selectedTheme)) {
			themeHandleForClick(MapViewProperties.getString("String_UniqueTheme_Function"), MapViewProperties.getString("String_UniqueTheme_Example"),
					ThemeType.UNIQUE, uniqueThemeLabel,
					InternalImageIconFactory.THEMEGUIDE_PREVIEW_UNIQUE);
		} else if (MapViewProperties.getString("String_Theme_Range").equals(selectedTheme)) {
			themeHandleForClick(MapViewProperties.getString("String_RangeTheme_Function"), MapViewProperties.getString("String_RangeTheme_Example"),
					ThemeType.RANGE, rangeThemeLabel,
					InternalImageIconFactory.THEMEGUIDE_PREVIEW_RANGE);
		} else if (MapViewProperties.getString("String_ThemeLabelUniformItem").equals(selectedTheme)
				|| MapViewProperties.getString("String_ThemeLabel").equals(selectedTheme)) {
			styleHandleForClick(uniformItem, MapViewProperties.getString("String_UniformTheme_Explain"),
					MapViewProperties.getString("String_UniformLabelTheme_Example"),
					uniqueStyelLabel,
					InternalImageIconFactory.THEMEGUIDE_PREVIEW_UNIFORM);
		} else if (MapViewProperties.getString("String_ThemeLabelRangeItem").equals(selectedTheme)) {
			styleHandleForClick(rangeItem, MapViewProperties.getString("String_RangeTheme_Explain"),
					MapViewProperties.getString("String_RangeLabelTheme_Example"), rangeStyleLabel,
					InternalImageIconFactory.THEMEGUIDE_PREVIEW_RANGES);
		} else if (MapViewProperties.getString("String_ThemeLabelMixedItem").equals(selectedTheme)) {
			styleHandleForClick(mixedItem, MapViewProperties.getString("String_ThemeLabelMixedItem"),
					MapViewProperties.getString("String_MixedLabelTheme_Example"),
					complicatedStyleLabel,
					InternalImageIconFactory.THEMEGUIDE_PREVIEW_COMPLICATED);
		} else if (MapViewProperties.getString("String_ThemeLabelAdvanceItem").equals(selectedTheme)) {
			styleHandleForClick(advanceItem, MapViewProperties.getString("String_ThemeLabelAdvanceItem"),
					MapViewProperties.getString("String_AdvanceLabelTheme_Example"), matrixStyleLabel,
					InternalImageIconFactory.THEMEGUIDE_PREVIEW_MATRIX);
		} else if (MapViewProperties.getString("String_Theme_Graph").equals(selectedTheme)) {
			themeHandleForClick(MapViewProperties.getString("String_GraphTheme_Function"), MapViewProperties.getString("String_Theme_Graph"),
					ThemeType.GRAPH, graphThemeLabel,
					InternalImageIconFactory.THEMEGUIDE_PREVIEW_GRAPH);
		} else if (MapViewProperties.getString("String_Theme_GraduatedSymbol").equals(selectedTheme)) {
			themeHandleForClick(MapViewProperties.getString("String_GraduatedSymbolTheme_Function"),
					MapViewProperties.getString("String_Theme_GraduatedSymbol"), ThemeType.GRADUATEDSYMBOL,
					graduatedThemeLabel,
					InternalImageIconFactory.THEMEGUIDE_PREVIEW_GRADUATEDSYMBOL);
		} else if (MapViewProperties.getString("String_Theme_DotDensity").equals(selectedTheme)) {
			themeHandleForClick(MapViewProperties.getString("String_DotDensityTheme_Function"), MapViewProperties.getString("String_Theme_DotDensity"),
					ThemeType.DOTDENSITY, dotDensityLabel,
					InternalImageIconFactory.THEMEGUIDE_PREVIEW_DOTDENSITY);
		} else if (MapViewProperties.getString("String_ThemeCustom").equals(selectedTheme)) {
			themeHandleForClick(MapViewProperties.getString("String_CustomTheme_Function"), MapViewProperties.getString("String_ThemeCustom"),
					ThemeType.CUSTOM, selfDefinitionLabel,
					InternalImageIconFactory.THEMEGUIDE_PREVIEW_CUSTOM);
		} else if (MapViewProperties.getString("String_ThemeModel").equals(selectedTheme)) {
			themeModuleHandleForClick();
		}
	}

	private void themeModuleHandleForClick() {
		// System.out.println("专题图模板");
	}

	/**
	 * @param textAreaString 功能描述
	 * @param title 专题图示例的title
	 * @param type 专题图类型
	 * @param label 专题图示例中的图片变量
	 * @param labelPNGName 专题图示例中的图片名称
	 */
	private void themeHandleForClick(String textAreaString, String title,
			ThemeType type, ThemeLabelIcon label, ImageIcon icon) {
		if (currentThemeType == null || !currentThemeType.equals(type)) {
			// 更改右上侧功能描述
			jTextArea.replaceRange(textAreaString, 0, jTextArea.getText()
					.length());

			// 设置当前专题图类型
			currentThemeType = type;
			currentLabelStyle = defualt_theme_style;

			// 设置专题图示例的title
			titledBorder.setTitle(title);

			// 更改右侧label
			if (label == null) {
				label = new ThemeLabelIcon(icon, new Dimension(
						jPanelRightCenter.getWidth(), jPanelRightCenter
								.getHeight()));
			}
			jPanelRightCenter.removeAll();
			jPanelRightCenter.add(label);
			jPanelRightCenter.revalidate();
			jPanelRightCenter.repaint();

			// 关闭曾经打开的标签专题图树节点
			if (jTree.isExpanded(2)) {
				jTree.collapseRow(2);
			}
		}
	}

	/**
	 * @param style 标签专题图风格
	 * @param textAreaString textArea文本
	 * @param title 示例title
	 * @param label 示例图片变量
	 * @param labelPNGName 示例图片
	 */
	private void styleHandleForClick(int style, String textAreaString,
			String title, ThemeLabelIcon label, ImageIcon icon) {
		if (currentLabelStyle == -1 || currentLabelStyle != style) {
			// 更改右上侧功能描述
			jTextArea.replaceRange(textAreaString, 0, jTextArea.getText().length());

			// 设置当前标签专题图风格类型
			currentLabelStyle = style;
			currentThemeType = ThemeType.LABEL;

			// 设置专题图示例的title
			titledBorder.setTitle(title);

			// 更改右侧label
			if (label == null) {
				label = new ThemeLabelIcon(icon, new Dimension(
						jPanelRightCenter.getWidth(), jPanelRightCenter
								.getHeight()));
			}
			jPanelRightCenter.removeAll();
			jPanelRightCenter.add(label);
			jPanelRightCenter.revalidate();
			jPanelRightCenter.repaint();
		}
	}

	private void getChildNode(DefaultMutableTreeNode root) {
		// 单值专题图
		uniqueNode = new DefaultMutableTreeNode(MapViewProperties.getString("String_Theme_Unique"));
		root.add(uniqueNode);

		// 分段专题图
		rangedNode = new DefaultMutableTreeNode(MapViewProperties.getString("String_Theme_Range"));
		root.add(rangedNode);

		// 标签专题图
		labelNode = new DefaultMutableTreeNode(MapViewProperties.getString("String_ThemeLabel"));
		root.add(labelNode);

		// 标签专题图的统一风格
		uniqueStyleOfLabelTheme = new DefaultMutableTreeNode(MapViewProperties.getString("String_ThemeLabelUniformItem"));
		labelNode.add(uniqueStyleOfLabelTheme);

		// 标签专题图的分段风格
		rangeStyleOfLabelTheme = new DefaultMutableTreeNode(MapViewProperties.getString("String_ThemeLabelRangeItem"));
		labelNode.add(rangeStyleOfLabelTheme);

		// // 标签专题图的复合风格
		// m_complicatedOfLabelTheme = new
		// DefaultMutableTreeNode(MapViewProperties.getString("String_ThemeLabelMixedItem"));
		// m_labelNode.add(m_complicatedOfLabelTheme);
		//
		// // 标签专题图的矩阵风格
		// m_matrixStyleOfLabelTheme = new
		// DefaultMutableTreeNode(MapViewProperties.getString("String_ThemeLabelAdvanceItem"));
		// m_labelNode.add(m_matrixStyleOfLabelTheme);

		// 统计专题图
		// m_graphNode = new
		// DefaultMutableTreeNode(MapViewProperties.getString("String_Theme_Graph"));
		// root.add(m_graphNode);

		// // 等级符号专题图
		// m_graduatedSymbolNode = new
		// DefaultMutableTreeNode(MapViewProperties.getString("String_Theme_GraduatedSymbol"));
		// root.add(m_graduatedSymbolNode);
		//
		// // 点密度专题图
		// m_dotDensityNode = new
		// DefaultMutableTreeNode(MapViewProperties.getString("String_Theme_DotDensity"));
		// root.add(m_dotDensityNode);
		//
		// // 自定义专题图
		// m_selfDefinitionNode = new
		// DefaultMutableTreeNode(MapViewProperties.getString("String_ThemeCustom"));
		// root.add(m_selfDefinitionNode);
		//
		// // 专题图模板
		// m_themeModule = new
		// DefaultMutableTreeNode(MapViewProperties.getString("String_ImportTheme"));
		// root.add(m_themeModule);
	}

	private JPanel getDownPanel() {
		if (jPanelDown == null) {
			jPanelDown = new JPanel();
			jPanelDown.setLayout(null);
			jPanelDown.setBounds(5, 365, 513, 30);
			add(jPanelDown);

			jButtonNext = new JButton();
			jButtonNext.setMargin(new Insets(0, 0, 0, 0));
			jButtonNext.setText(CommonProperties.getString("String_Button_Next"));
			jButtonNext.setBounds(295, 5, 70, 18);
			jButtonNext.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// 设置title
					title = new StringBuilder();
					if (mapOfThemeType.get(currentThemeType).equals(MapViewProperties.getString("String_ThemeLabel"))) {
						title.append(subMapOfThemeLabelStyle.get(currentLabelStyle)).append(mapOfThemeType.get(currentThemeType));
					} else {
						title.append(mapOfThemeType.get(currentThemeType));
					}
					m_themeGuide.setTitle(title.toString());
					// 输出信息
					// System.out.println("title: " + title.toString());
					// System.out.println("m_currentThemeType: "
					// + m_currentThemeType);
					// System.out.println("m_currentLabelStyle: "
					// + m_currentLabelStyle);
					// System.out.println("m_currentPanel: " + m_currentPanel);

					// 判断是否重新选择专题图
					// if (m_recordLabelStyle != m_currentLabelStyle ||
					// m_recordThemeType != m_currentThemeType) {
					if (basicThemePanel != null) {
						basicThemePanel.disposeAll();
						basicThemePanel = null;
					}
					// }

					// 第一次加载m_basicThemePanel或重新选择专题图类型后加载的
					if (basicThemePanel == null) {
						parent = (JPanel) ThemeMainPanel.this.getParent();
						basicThemePanel = new BasicThemePanel(
								currentThemeType, currentLabelStyle,
								dataset, jPanelCurrent, container);
						recordLabelStyle = currentLabelStyle;
						recordThemeType = currentThemeType;
					}
					parent.remove(ThemeMainPanel.this);
					parent.add(basicThemePanel);
					parent.revalidate();
					parent.repaint();
				}
			});
			jPanelDown.add(jButtonNext);

			jButtonOk = new JButton();
			jButtonOk.setMargin(new Insets(0, 0, 0, 0));
			jButtonOk.setText(CommonProperties.getString("String_Button_Cancel"));
			jButtonOk.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					m_themeGuide.setVisible(false);
					container.dispose();
				}
			});
			jButtonOk.setBounds(380, 5, 70, 18);
			jPanelDown.add(jButtonOk);
		}
		return jPanelDown;
	}

	/**
	 * 向导对话框传递到子Panel
	 * 
	 * @return m_themeGuide
	 */
	ThemeGuide getGuide() {
		return m_themeGuide;
	}

	/**
	 * 树的渲染类
	 */
	private class TreeCellRenderImp extends JButton implements TreeCellRenderer {
		private Color defaultColor = new Color(238, 238, 238);
		private Color selectedForeColor = new Color(112, 133, 234);

		public TreeCellRenderImp() {
			super();
			setOpaque(true);
			setMargin(new Insets(2, 2, 2, 2));
			setBorder(null);
			this.setIconTextGap(5);
			this.setBorder(BorderFactory.createEmptyBorder(10, 1, 10, 1));
		}

		public Component getTreeCellRendererComponent(JTree tree, Object value,
				boolean selected, boolean expanded, boolean leaf, int row,
				boolean hasFocus) {
			// isEnable 与 tree 保持一致
			this.setEnabled(tree.isEnabled());

			// 保证按钮前景色与背景色与tree保持一致
			if (selected && !value.toString().equals(MapViewProperties.getString("String_ThemeLabel"))) {
				this.setForeground(Color.WHITE);
				this.setBackground(selectedForeColor);
			} else {
				this.setBackground(defaultColor);
				this.setForeground(Color.BLACK);
			}

			String themeName = value.toString();
			// 根据不同的专题图类型选用不同的图标
			ImageIcon icon = null;
			if (MapViewProperties.getString("String_Theme_Unique").equals(themeName)) {
				icon = InternalImageIconFactory.THEMEGUIDE_UNIQUE;
				this.setText(MapViewProperties.getString("String_Theme_Unique"));
			} else if (MapViewProperties.getString("String_Theme_Range").equals(themeName)) {
				icon = InternalImageIconFactory.THEMEGUIDE_RANGE;
				this.setText(MapViewProperties.getString("String_Theme_Range"));
			} else if (MapViewProperties.getString("String_ThemeLabel").equals(themeName)) {
				icon = InternalImageIconFactory.THEMEGUIDE_LABEL;
				this.setText(MapViewProperties.getString("String_ThemeLabel"));
			} else if (MapViewProperties.getString("String_Theme_Graph").equals(themeName)) {
				icon = InternalImageIconFactory.THEMEGUIDE_GRAPH;
				this.setText(MapViewProperties.getString("String_Theme_Graph"));
			} else if (MapViewProperties.getString("String_Theme_GraduatedSymbol").equals(themeName)) {
				icon = InternalImageIconFactory.THEMEGUIDE_GRADUATEDSYMBOL;
				this.setText(MapViewProperties.getString("String_Theme_GraduatedSymbol"));
			} else if (MapViewProperties.getString("String_Theme_DotDensity").equals(themeName)) {
				icon = InternalImageIconFactory.THEMEGUIDE_DOTDENSITY;
				this.setText(MapViewProperties.getString("String_Theme_DotDensity"));
			} else if (MapViewProperties.getString("String_ThemeLabelUniformItem").equals(themeName)) {
				icon = InternalImageIconFactory.THEMEGUIDE_UNIFORM;
				this.setText(MapViewProperties.getString("String_ThemeLabelUniformItem"));
			} else if (MapViewProperties.getString("String_ThemeLabelRangeItem").equals(themeName)) {
				icon = InternalImageIconFactory.THEMEGUIDE_RANGES;
				this.setText(MapViewProperties.getString("String_ThemeLabelRangeItem"));
			} else if (MapViewProperties.getString("String_ThemeLabelMixedItem").equals(themeName)) {
				icon = InternalImageIconFactory.THEMEGUIDE_COMPLICATED;
				this.setText(MapViewProperties.getString("String_ThemeLabelMixedItem"));
			} else if (MapViewProperties.getString("String_ThemeLabelAdvanceItem").equals(themeName)) {
				icon = InternalImageIconFactory.THEMEGUIDE_MATRIX;
				this.setText(MapViewProperties.getString("String_ThemeLabelAdvanceItem"));
			} else if (MapViewProperties.getString("String_ThemeCustom").equals(themeName)) {
				icon = InternalImageIconFactory.THEMEGUIDE_CUSTOM;
				this.setText(MapViewProperties.getString("String_ThemeCustom"));
			} else if (MapViewProperties.getString("String_ImportTheme").equals(themeName)) {
				icon = InternalImageIconFactory.THEMEGUIDE_MODULE;
				this.setText(MapViewProperties.getString("String_ImportTheme"));
			}
			this.setIcon(icon);

			// 当点击键盘"up" or "down" execute the statement
			// isMouseClicked 用于解决键盘和鼠标之间的冲突
			if (!isMouseClicked) {
				if (selected) {
					TreePath path = jTree.getSelectionPath();
					if (path.getLastPathComponent().toString().equals(MapViewProperties.getString("String_ThemeLabel"))) {
						if (this.getText().equals(MapViewProperties.getString("String_ThemeLabel"))) {
							// 该段代码的功能为 当从选中统一风格时，按up按钮，直接跳到分段专题图
							// TreeNode parent =
							// (TreeNode)path.getParentPath().getLastPathComponent();
							// TreePath newPath =
							// path.getParentPath().pathByAddingChild(m_tree.getModel().getChild(parent,
							// 1));
							// m_tree.setSelectionPath(newPath);

							path = path.pathByAddingChild(jTree.getModel().getChild(path.getLastPathComponent(), 0));
							jTree.setSelectionPath(path);
						}
						if (!expanded) {
							// 点击标签专题图类型时,判断曾经点击过的是标签子风格 or 其他专题图类型
							// 其他专题图
							if (!recordPath.getParentPath()
									.getLastPathComponent().toString().equals(
											MapViewProperties.getString("String_ThemeLabel"))) {
								path = path.pathByAddingChild(jTree.getModel()
										.getChild(path.getLastPathComponent(),
												0));
								recordPath = path;
							}
							jTree.setSelectionPath(recordPath);
							jTree.expandPath(recordPath);
						}
					} else {
						recordPath = path;
					}
					String selectedTheme = recordPath.getLastPathComponent().toString();
					refreshView(selectedTheme);
				}
			}
			return this;
		}
	}

	private class ThemeLabelIcon extends JLabel {
		public ThemeLabelIcon(ImageIcon icon, Dimension dimension) {
			setIcon(icon);
			setIconTextGap(5);
			setBorder(null);
			setSize((int) dimension.getWidth(), (int) dimension.getHeight());
		}
	}
}
