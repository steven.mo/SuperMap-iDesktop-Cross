package com.supermap.desktop.theme;

import java.util.ArrayList;

public class ThemeRangeThemeInfo extends ThemeInfo{
	public ThemeRangeThemeInfo(){
		list = new ArrayList<Class<?>>();
		list.add(ThemeRangeAttributePanel.class);
	}
}
